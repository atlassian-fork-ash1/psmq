package com.atlassian.psmq.internal.visiblefortesting;

/**
 * A backdoor interface to help with testing and testing ONLY
 */
public interface QueueResetter
{
    void eraseQs();

    void eraseQ(long queueId);
}

package com.atlassian.psmq.backdoor.internal.tests;

import com.atlassian.fugue.Option;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.message.QMessageConsumer;
import com.atlassian.psmq.api.message.QMessageProducer;
import com.atlassian.psmq.api.queue.QTopic;
import com.atlassian.psmq.api.queue.Queue;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 */
public class TestApiTopicPublishing extends BaseApiTest {
    public static final String TRAIN_LOVE = "Trains are unloved by the public and governments alike";

    @Test
    public void test_basic_topic_distribution() {
        // assemble
        QTopic topic = QTopic.newTopic("trainSpotting");
        Queue trainSpotter1 = Fixtures.createQ(qSession, Option.some(Fixtures.rqName()), Option.some(topic));
        Queue trainSpotter2 = Fixtures.createQ(qSession, Option.some(Fixtures.rqName()), Option.some(topic));
        Queue trainSpotter3 = Fixtures.createQ(qSession, Option.some(Fixtures.rqName()), Option.some(topic));
        Queue nonTrainSpotter = Fixtures.createQ(qSession, Option.some(Fixtures.rqName()), Option.none()); // does not like trains
        List<Queue> trainSpotters = Lists.newArrayList(trainSpotter1, trainSpotter2, trainSpotter3, nonTrainSpotter);

        // act
        QMessageProducer messageProducer = qSession.createProducer(topic);

        QMessage trainMessage = Fixtures.basicMsg(TRAIN_LOVE);
        long writtenTo = messageProducer.writeMessage(trainMessage);

        List<Optional<QMessage>> readMessages = Lists.newArrayList();
        for (Queue queue : trainSpotters) {
            QMessageConsumer messageConsumer = qSession.createConsumer(queue);
            Optional<QMessage> readMsg = messageConsumer.claimAndResolve();
            if (readMsg.isPresent()) {
                readMessages.add(readMsg);
            }
        }

        // assert
        assertThat(writtenTo, equalTo(3L));
        assertThat(readMessages.size(), equalTo(3));
        for (Optional<QMessage> readMessage : readMessages) {
            assertThat(readMessage.isPresent(), equalTo(true));
            QMessage msg = readMessage.get();
            assertThat(msg.buffer().asString(), equalTo(TRAIN_LOVE));
        }
    }

}

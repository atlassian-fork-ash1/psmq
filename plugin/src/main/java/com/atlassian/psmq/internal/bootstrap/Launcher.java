package com.atlassian.psmq.internal.bootstrap;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.psmq.internal.util.logging.LogLeveller;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * The Launcher is the starting point for the plugin.
 *
 * This should be the only life cycle listener in the system and it should then delegate out to other code as
 * appropriate.
 *
 * Its purpose is to allow us to reason about where events enter the system and the order of called code.  Having a
 * single listener / entry point object allows us to do that.
 */
@ExportAsService
@Component
public class Launcher implements LifecycleAware {
    private static final Logger log = getLogger(Launcher.class);

    public Launcher() {
        //
        // we want our logs to go out as info even if the host defaults to warn
        LogLeveller.setInfo(log);
    }

    @Override
    public void onStart() {
        onStartCompleted();
    }

    @Override
    public void onStop() {
    }

    @PostConstruct
    public void onSpringContextStarted() {
        log.info("PSMQ spring context is starting...");
    }

    @VisibleForTesting
    void onStartCompleted() {
        log.info("PSMQ is initializing...");

        log.info("PSMQ is initialized.");
    }

    @PreDestroy
    public void onSpringContextStopped() {
        log.info("PSMQ spring context is stopping...");

        log.info("PSMQ spring context is stopped.");
    }
}

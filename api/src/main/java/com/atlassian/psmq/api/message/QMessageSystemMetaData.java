package com.atlassian.psmq.api.message;

import java.util.Date;

/**
 * System related meta data about a given {@link com.atlassian.psmq.api.message.QMessage}
 *
 * @since v0.x
 */
public interface QMessageSystemMetaData
{
    /**
     * @return the system id
     */
    QSystemId systemId();

    /**
     * @return the message claimant
     */
    QClaimant claimant();

    /**
     * @return the date the message was created
     */
    Date createdDate();

    /**
     * @return the number of times this message has been claimed
     */
    int claimCount();

}

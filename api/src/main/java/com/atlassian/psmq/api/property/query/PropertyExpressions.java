package com.atlassian.psmq.api.property.query;


import com.atlassian.annotations.Internal;
import com.google.common.base.Objects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Base classes for property query expressions
 *
 * @since 1.0
 */
@Internal
public class PropertyExpressions
{
    public interface Visitor<R, C>
    {
        R visit(LongConstant expr, C context);

        R visit(StringConstant expr, C context);
    }

    public interface PropertyExpression<T>
    {
        @Nonnull
        T getValue();

        @SuppressWarnings ("UnusedDeclaration")
        <R, C> R accept(Visitor<R, C> visitor, C context);
    }


    public abstract static class BasePropertyExpression<T> implements PropertyExpression<T>
    {
        private final T value;

        protected BasePropertyExpression(final T value)
        {
            this.value = value;
        }

        @Nonnull
        @Override
        public T getValue()
        {
            return value;
        }

        @Override
        public String toString()
        {
            return Objects.toStringHelper(this).add("value", value).toString();
        }
    }

    public static class StringConstant extends BasePropertyExpression<String>
    {
        public StringConstant(final String value)
        {
            super(value);
        }

        @Override
        public <R, C> R accept(final Visitor<R, C> visitor, @Nullable final C context)
        {
            return visitor.visit(this, context);
        }

    }

    public static class LongConstant extends BasePropertyExpression<Long>
    {
        public LongConstant(final long value)
        {
            super(value);
        }

        @Override
        public <R, C> R accept(final Visitor<R, C> visitor, @Nullable final C context)
        {
            return visitor.visit(this, context);
        }
    }
}




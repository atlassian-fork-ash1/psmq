package com.atlassian.psmq.api;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.QSessionDefinition.CommitStrategy;
import com.atlassian.psmq.api.internal.Validations;
import com.atlassian.psmq.api.message.QClaimant;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static com.atlassian.psmq.api.QSessionDefinition.CommitStrategy.AUTO_COMMIT;
import static com.atlassian.psmq.api.QSessionDefinition.CommitStrategy.EXTERNAL_COMMIT;
import static com.atlassian.psmq.api.QSessionDefinition.CommitStrategy.SESSION_COMMIT;
import static com.atlassian.psmq.api.internal.Validations.checkNotNull;

/**
 * A builder of {@link com.atlassian.psmq.api.QSessionDefinition}s
 *
 * @since v1.0
 */
@PublicApi
public class QSessionDefinitionBuilder {
    /**
     * The default session heart beat if you don't provide one is 5 minutes
     */
    public static final long DEFAULT_SESSION_HEARTBEAT = TimeUnit.MINUTES.toMillis(5);

    /**
     * The default commit strategy is auto-commit
     */
    public static final CommitStrategy DEFAULT_COMMIT_STRATEGY = AUTO_COMMIT;

    CommitStrategy commitStrategy = DEFAULT_COMMIT_STRATEGY;
    Optional<QConnectionProvider> connectionProvider = Optional.empty();
    Optional<Runnable> closeRunnable = Optional.empty();
    QClaimant claimant = new QClaimant();
    long claimantHeartBeat = DEFAULT_SESSION_HEARTBEAT;

    public static QSessionDefinitionBuilder newDefinition() {
        return new QSessionDefinitionBuilder();
    }

    public QSessionDefinitionBuilder withAutoCommitStrategy() {
        this.commitStrategy = AUTO_COMMIT;
        return this;
    }

    public QSessionDefinitionBuilder withSessionCommitStrategy(QConnectionProvider connectionProvider) {
        this.connectionProvider = Optional.of(checkNotNull(connectionProvider));
        this.commitStrategy = SESSION_COMMIT;
        return this;
    }

    public QSessionDefinitionBuilder withExternalCommitStrategy(QConnectionProvider connectionProvider) {
        this.connectionProvider = Optional.of(checkNotNull(connectionProvider));
        this.commitStrategy = EXTERNAL_COMMIT;
        return this;
    }

    /**
     * A session can act as a specific 'claimant' of messages.
     *
     * @param claimant the claimant to act as in this session
     *
     * @return the builder
     */
    public QSessionDefinitionBuilder withClaimant(QClaimant claimant) {
        this.claimant = checkNotNull(claimant);
        return this;
    }

    /**
     * A session can act as a specific 'claimant' of messages and can require that
     * they cause a heart beat every N milliseconds.
     *
     * @param claimant                the claimant to act as in this session
     * @param claimantHeartBeatMillis the minimum milliseconds in which a claimant should cause a heart beat
     *
     * @return the builder
     */
    public QSessionDefinitionBuilder withClaimant(QClaimant claimant, long claimantHeartBeatMillis) {
        Validations.checkArgument(claimantHeartBeatMillis > 0, "You must provide a sensible heartbeat value");
        this.claimant = checkNotNull(claimant);
        this.claimantHeartBeat = claimantHeartBeatMillis;
        return this;
    }

    /**
     * This runnable will be called when the defined QSession is closed
     *
     * @param onCloseRunnable a runnable
     *
     * @return the builder
     */
    public QSessionDefinitionBuilder withOnClose(Runnable onCloseRunnable) {
        this.closeRunnable = Optional.of(checkNotNull(onCloseRunnable));
        return this;
    }


    public QSessionDefinition build() {
        return new QSessionDefinition() {
            @Override
            public QClaimant claimant() {
                return claimant;
            }

            @Override
            public long claimantHeartBeatMillis() {
                return claimantHeartBeat;
            }

            @Override
            public CommitStrategy commitStrategy() {
                return commitStrategy;
            }

            @Override
            public Optional<QConnectionProvider> connectionProvider() {
                return connectionProvider;
            }

            @Override
            public Optional<Runnable> onClose() {
                return closeRunnable;
            }
        };
    }
}

package com.atlassian.psmq.api;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.message.QClaimant;

import java.util.Optional;

/**
 * This control how the queueing session is configured
 *
 * @since v1.0
 */
@PublicApi
public interface QSessionDefinition {
    /**
     * @return the commit strategy in play
     */
    CommitStrategy commitStrategy();

    /**
     * A session acts as the claimant of read messages.
     *
     * @return this sessions QClaimant value
     */
    QClaimant claimant();

    /**
     * A claimant should make a heart beat every so often otherwise any claim they have will be considered null and void
     *
     * @return the time in ms in which a heart beat should be made
     */
    long claimantHeartBeatMillis();

    /**
     * @return an optional external connection provider, only needed if you use {@link com.atlassian.psmq.api.QSessionDefinition.CommitStrategy#EXTERNAL_COMMIT}
     * or {@link com.atlassian.psmq.api.QSessionDefinition.CommitStrategy#SESSION_COMMIT} strategy
     */
    Optional<QConnectionProvider> connectionProvider();

    /**
     * @return an optional runnable that will run when the session is closed
     */
    Optional<Runnable> onClose();

    /**
     * These are the commit strategies that can be used when reading and writing messages
     */
    enum CommitStrategy {
        /**
         * After each message is read or written it will be committed as such
         */
        AUTO_COMMIT,
        /**
         * The {@link QSession#commit()} and  {@link QSession#rollback()} methods are used to control message committing
         */
        SESSION_COMMIT,
        /**
         * The commit happens outside the {@link com.atlassian.psmq.api.QSession}
         */
        EXTERNAL_COMMIT
    }


}

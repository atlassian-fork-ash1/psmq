package com.atlassian.psmq.api.message;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.internal.Validations;

/**
 * {@link com.atlassian.psmq.api.message.QMessage}s can have a content type to help you interpret them when reading them
 *
 * @since v1.0
 */
@PublicApi
public class QContentType
{
    public static final QContentType UNSPECIFIED = new QContentType("UNSPECIFIED");
    public static final QContentType JSON = new QContentType("application/json");
    public static final QContentType XML = new QContentType("application/xml");
    public static final QContentType PLAIN_TEXT = new QContentType("application/text");

    private final String value;

    public QContentType(final String value)
    {
        this.value = Validations.checkQContentType(value);
    }

    /**
     * @return the content type value
     */
    public String value()
    {
        return value;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        final QContentType that = (QContentType) o;

        if (!value.equals(that.value))
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        return value.hashCode();
    }
}

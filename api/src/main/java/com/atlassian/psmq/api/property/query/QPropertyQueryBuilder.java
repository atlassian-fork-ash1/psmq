package com.atlassian.psmq.api.property.query;

import com.atlassian.annotations.PublicApi;

/**
 * Builds queries that can be used to select queues and messages with certain {@link
 * com.atlassian.psmq.api.property.QProperty}s
 *
 * @since 1.0
 */
@PublicApi
public class QPropertyQueryBuilder
{
    /**
     * This starts a query by property name and then allows you to put in a value predicate afterwards
     *
     * @param name the name of the property
     * @return a query part that can then have a value predicate applied
     */
    public static NamedPropertyQueryPart named(String name)
    {
        return new NamedPropertyQueryPart(name);
    }

    /**
     * Joins two queries with 'and' logic
     *
     * @param left the left query
     * @param right the right query
     * @return the 'and-ed' result
     */
    public static QPropertyQuery and(QPropertyQuery left, QPropertyQuery right)
    {
        return new QPropertyQuery(QPropertyQuery.Operator.AND, left, right);
    }

    /**
     * Joins two queries with 'or' logic
     *
     * @param left the left query
     * @param right the right query
     * @return the 'or-ed' result
     */
    public static QPropertyQuery or(QPropertyQuery left, QPropertyQuery right)
    {
        return new QPropertyQuery(QPropertyQuery.Operator.OR, left, right);
    }
}

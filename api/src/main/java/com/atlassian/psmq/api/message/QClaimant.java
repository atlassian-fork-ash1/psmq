package com.atlassian.psmq.api.message;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.internal.Validations;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * This represents a specific process / entity that is claiming messages.  It is tied to a specific {@link
 * com.atlassian.psmq.api.QSession} and then messages read within that session will be claimed in that name.
 *
 * @since v1.0
 */
@PublicApi
public class QClaimant
{
    private final String value;

    public static QClaimant claimant(String value)
    {
        return new QClaimant(value);
    }

    public QClaimant(final String value)
    {
        this.value = Validations.checkQClaimant(value);
    }

    public QClaimant()
    {
        this(computeClaimant());
    }

    /**
     * @return the name of the claimant
     */
    public String value()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return value;
    }

    private static String computeClaimant()
    {
        String time = new SimpleDateFormat("'claimant-'YYYYMMDDHHmmssSSSz").format(new Date());
        String uuid = UUID.randomUUID().toString();
        return time + "-" + uuid;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        final QClaimant that = (QClaimant) o;

        return value.equals(that.value);
    }

    @Override
    public int hashCode()
    {
        return value.hashCode();
    }
}

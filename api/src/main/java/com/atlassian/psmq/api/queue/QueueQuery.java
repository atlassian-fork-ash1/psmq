package com.atlassian.psmq.api.queue;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.paging.LimitedPageRequest;
import com.atlassian.psmq.api.property.query.QPropertyQuery;

import java.util.Optional;

/**
 * Criteria for finding queues
 *
 * @since v1.0
 */
@PublicApi
public interface QueueQuery {
    /**
     * @return the queue id to filter on
     */
    Optional<QId> queueId();

    /**
     * @return the queue name to filter on
     */
    Optional<String> queueName();

    /**
     * @return the topic to filter on
     */
    Optional<QTopic> topic();

    /**
     * @return the property query to filter on
     */
    Optional<QPropertyQuery> properties();

    /**
     * @return the paging to apply to this query
     */
    LimitedPageRequest paging();
}

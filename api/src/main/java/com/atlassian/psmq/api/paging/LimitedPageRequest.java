package com.atlassian.psmq.api.paging;


import com.atlassian.annotations.PublicApi;

/**
 * A {@link PageRequest} guaranteed to have a limit capped to a developer provided value rather than a user provided
 * value. getLimit is restricted to the minimum of getMaxLimit and the user provided value.  Does not support requesting
 * unlimited result sets.
 */
@PublicApi
public interface LimitedPageRequest
{
    /**
     * start index for the page of results
     */
    int getStart();

    /**
     * @return the maximum results to fetch to attempt to fulfil the request
     */
    int getLimit();

    /**
     * @return the maximum limit for this request
     */
    int getMaxLimit();
}

package com.atlassian.psmq.api.paging;


import com.atlassian.annotations.PublicApi;
import com.google.common.base.Objects;

@PublicApi
public class LimitedPageRequestImpl implements LimitedPageRequest
{
    private final int start;
    private final int limit;
    private final int maxLimit;

    public static LimitedPageRequest create(PageRequest request, int maxLimit)
    {
        return new LimitedPageRequestImpl(request, maxLimit);
    }

    public static LimitedPageRequest create(int maxLimit)
    {
        return new LimitedPageRequestImpl(0, maxLimit, maxLimit);
    }

    public static LimitedPageRequest create(int start, int limit, int maxLimit)
    {
        return new LimitedPageRequestImpl(start, limit, maxLimit);
    }

    protected LimitedPageRequestImpl(PageRequest request, int maxLimit)
    {
        this(request.getStart(), request.getLimit(), request.getLimit(), maxLimit);
    }

    protected LimitedPageRequestImpl(int start, int limit, int maxLimit)
    {
        this(start, Math.min(limit, maxLimit), Math.min(limit, maxLimit), maxLimit);
    }

    protected LimitedPageRequestImpl(int start, int limit, int needed, int maxLimit)
    {
        if (maxLimit == Integer.MAX_VALUE || maxLimit < 0)
        { throw new IllegalArgumentException(maxLimit + " is not a sensible max limit"); }

        if (limit < 0)
        { throw new IllegalArgumentException("limit cannot be less than zero"); }

        if (start < 0)
        {
            throw new IllegalArgumentException("start cannot be less than zero");
        }

        this.start = start;
        this.limit = Math.max(0, Math.min(limit, maxLimit));

        this.maxLimit = maxLimit;
    }

    @Override
    public int getStart()
    {
        return start;
    }

    @Override
    public int getLimit()
    {
        return limit;
    }

    @Override
    public int getMaxLimit()
    {
        return maxLimit;
    }

    @Override
    public String toString()
    {
        return Objects.toStringHelper(this).add("start", start).add("limit", limit)
                .add("maxLimit", maxLimit).toString();
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(limit, maxLimit, start);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null || !LimitedPageRequest.class.isAssignableFrom(obj.getClass()))
        {
            return false;
        }

        LimitedPageRequest other = (LimitedPageRequest) obj;

        return Objects.equal(limit, other.getLimit()) &&
                Objects.equal(maxLimit, other.getMaxLimit()) &&
                Objects.equal(start, other.getStart());
    }
}

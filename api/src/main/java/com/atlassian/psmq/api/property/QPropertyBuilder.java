package com.atlassian.psmq.api.property;

import com.atlassian.annotations.PublicApi;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.joda.time.format.ISODateTimeFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

import static com.atlassian.psmq.api.internal.Validations.checkNotNull;
import static com.atlassian.psmq.api.internal.Validations.checkPropertyDate;
import static com.atlassian.psmq.api.internal.Validations.checkPropertyName;
import static com.atlassian.psmq.api.internal.Validations.checkPropertyValue;
import static com.atlassian.psmq.api.internal.Validations.checkState;
import static java.util.Optional.ofNullable;

/**
 * A builder of {@link QProperty}s.
 *
 * @since v1.0
 */
@PublicApi
public class QPropertyBuilder {
    QPropertyImpl property = null;

    public QPropertyBuilder() {
    }

    public static QPropertyBuilder newProperty() {
        return new QPropertyBuilder();
    }

    public static QPropertySet newPropertySet(Set<QProperty> properties) {
        return new QPropertySetImpl(properties);
    }

    public QPropertyBuilder with(String name, String value) {
        this.property = new QPropertyImpl(name, value);
        return this;
    }

    public QPropertyBuilder with(String name, long value) {
        this.property = new QPropertyImpl(name, value);
        return this;
    }

    public QPropertyBuilder with(String name, boolean value) {
        this.property = new QPropertyImpl(name, value);
        return this;
    }

    public QPropertyBuilder with(String name, Date value) {
        this.property = new QPropertyImpl(name, value);
        return this;
    }

    public QProperty build() {
        checkState(property != null, "You must specify some values into the property");
        return property;
    }


    private static class QPropertyImpl implements QProperty {
        private final Type type;
        private final String name;
        private final String s;
        private final long l;
        private final boolean b;
        private final Date date;

        public QPropertyImpl(final String name, final String value) {
            this.type = Type.STRING;
            this.name = checkPropertyName(name);
            this.s = checkPropertyValue(value);
            this.l = 0;
            this.b = false;
            this.date = new Date(0);
        }

        public QPropertyImpl(final String name, final long l) {
            this.type = Type.LONG;
            this.name = checkPropertyName(name);
            this.s = "";
            this.l = l;
            this.b = false;
            this.date = new Date(0);
        }

        public QPropertyImpl(final String name, final boolean b) {
            this.type = Type.BOOLEAN;
            this.name = checkPropertyName(name);
            this.s = "";
            this.l = 0;
            this.b = b;
            this.date = new Date(0);
        }

        public QPropertyImpl(final String name, final Date date) {
            this.type = Type.DATE;
            this.name = checkPropertyName(name);
            this.s = "";
            this.l = 0;
            this.b = false;
            this.date = checkPropertyDate(date);
        }

        @Override
        public Type type() {
            return type;
        }

        @Override
        public String asString() {
            switch (type) {
                case STRING:
                    return s;
                case LONG:
                    return String.valueOf(l);
                case BOOLEAN:
                    return String.valueOf(b);
                case DATE:
                    return ISODateTimeFormat.dateTime().print(date.getTime());
            }
            return s;
        }

        @Override
        public String name() {
            return name;
        }

        @Override
        public String stringValue() {
            return s;
        }

        @Override
        public long longValue() {
            return l;
        }

        @Override
        public boolean booleanValue() {
            return b;
        }

        @Override
        public Date dateValue() {
            return date;
        }


        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("type", type())
                    .append("string", stringValue())
                    .append("long", longValue())
                    .append("boolean", booleanValue())
                    .append("date", dateValue())
                    .toString();
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final QPropertyImpl that = (QPropertyImpl) o;

            return name.equals(that.name);
        }

        @Override
        public int hashCode() {
            return name.hashCode();
        }
    }

    private static class QPropertySetImpl implements QPropertySet {
        private final Set<QProperty> properties;
        private final Map<String, QProperty> propertiesMap;

        public QPropertySetImpl(final Set<QProperty> properties) {
            this.properties = new HashSet<>(checkNotNull(properties));
            this.propertiesMap = new HashMap<>();
            for (QProperty property : properties) {
                propertiesMap.put(property.name(), property);
            }
        }

        private static Predicate<QProperty> predicate(QProperty.Type type) {
            return p -> p.type().equals(type);
        }

        @Override
        public Set<QProperty> asSet() {
            return new HashSet<>(properties);
        }

        @Override
        public Set<String> names() {
            return propertiesMap.keySet();
        }

        @Override
        public Optional<QProperty> property(final String name) {
            return ofNullable(propertiesMap.get(name));
        }

        @Override
        public Optional<String> stringValue(final String name) {
            return ofNullable(propertiesMap.get(name)).filter(predicate(QProperty.Type.STRING)).map(p -> p.stringValue());
        }

        @Override
        public Optional<Long> longValue(final String name) {
            return ofNullable(propertiesMap.get(name)).filter(predicate(QProperty.Type.LONG)).map(p -> p.longValue());
        }

        @Override
        public Optional<Date> dateValue(final String name) {
            return ofNullable(propertiesMap.get(name)).filter(predicate(QProperty.Type.DATE)).map(p -> p.dateValue());
        }

        @Override
        public Optional<Boolean> booleanValue(final String name) {
            return ofNullable(propertiesMap.get(name)).filter(predicate(QProperty.Type.BOOLEAN)).map(p -> p.booleanValue());
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("properties", properties)
                    .toString();
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final QPropertySetImpl that = (QPropertySetImpl) o;

            return properties.equals(that.properties);

        }

        @Override
        public int hashCode() {
            return properties.hashCode();
        }
    }

}

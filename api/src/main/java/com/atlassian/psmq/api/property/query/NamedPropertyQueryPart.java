package com.atlassian.psmq.api.property.query;

import com.atlassian.annotations.PublicApi;
import com.google.common.base.Objects;

/**
 * Allows you to build queries on the properties that are associated with queues and messages.  You can chain
 * expressions together to create complex logic.
 *
 * @since v1.0
 */
@PublicApi
public class NamedPropertyQueryPart
{
    private final String name;

    NamedPropertyQueryPart(final String name)
    {
        this.name = name;
    }

    PropertyExpressions.StringConstant name()
    {
        return new PropertyExpressions.StringConstant(name);
    }

    // logical
    public QPropertyQuery eq(long value)
    {
        return longExp(value, QPropertyQuery.Operator.EQ);
    }

    public QPropertyQuery eq(String value)
    {
        return stringExp(value, QPropertyQuery.Operator.EQ);
    }

    public QPropertyQuery ne(long value)
    {
        return longExp(value, QPropertyQuery.Operator.NE);
    }

    public QPropertyQuery ne(String value)
    {
        return stringExp(value, QPropertyQuery.Operator.NE);
    }

    public QPropertyQuery isNotNull()
    {
        return new QPropertyQuery(QPropertyQuery.Operator.IS_NOT_NULL, name());
    }

    public QPropertyQuery isNull()
    {
        return new QPropertyQuery(QPropertyQuery.Operator.IS_NULL, name());
    }

    // comparable

    public QPropertyQuery goe(long value)
    {
        return longExp(value, QPropertyQuery.Operator.GOE);
    }

    public QPropertyQuery gt(long value)
    {
        return longExp(value, QPropertyQuery.Operator.GT);
    }

    public QPropertyQuery loe(long value)
    {
        return longExp(value, QPropertyQuery.Operator.LOE);
    }

    public QPropertyQuery lt(long value)
    {
        return longExp(value, QPropertyQuery.Operator.LT);
    }

    // string

    public QPropertyQuery eqIgnoreCase(String value)
    {
        return stringExp(value, QPropertyQuery.Operator.EQ_IGNORE_CASE);
    }

    public QPropertyQuery contains(String value)
    {
        return stringExp(value, QPropertyQuery.Operator.CONTAINS);
    }

    public QPropertyQuery startsWith(String value)
    {
        return stringExp(value, QPropertyQuery.Operator.STARTS_WITH);
    }

    public QPropertyQuery endsWith(String value)
    {
        return stringExp(value, QPropertyQuery.Operator.ENDS_WITH);
    }

    public QPropertyQuery like(String value)
    {
        return stringExp(value, QPropertyQuery.Operator.LIKE);
    }


    // impl

    private QPropertyQuery longExp(final long value, QPropertyQuery.Operator operator)
    {
        return new QPropertyQuery(operator, name(), new PropertyExpressions.LongConstant(value));
    }

    private QPropertyQuery stringExp(final String value, QPropertyQuery.Operator operator)
    {
        return new QPropertyQuery(operator, name(), new PropertyExpressions.StringConstant(value));
    }

    @Override
    public String toString()
    {
        return Objects.toStringHelper(this).add("name", name).toString();
    }
}

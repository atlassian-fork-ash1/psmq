package com.atlassian.psmq.api.internal;

import com.atlassian.annotations.Internal;
import com.atlassian.psmq.api.QException;
import com.atlassian.psmq.api.queue.Queue;
import com.atlassian.psmq.api.queue.QueueDefinition;
import com.google.common.base.Preconditions;

import javax.annotation.Nullable;
import java.util.Date;

/**
 * A helper class of Validations that throw pre-condition exceptions.  This is NOT API.
 */
@Internal
public class Validations
{
    // Queues
    public static String checkQueueName(String value)
    {
        return check450("Queue Name", value);
    }

    public static String checkQueuePurpose(String value)
    {
        return check450("Queue Purpose", value);
    }

    public static String checkQTopic(String value)
    {
        return check450("QTopic", value);
    }


    public static void checkQueueToItsDefinition(final Queue returnedQ, final QueueDefinition queueDefinition)
            throws QException
    {
        if (!queueDefinition.name().equals(returnedQ.name()))
        {
            throw new QException(String.format("The existing queue name '%s' does not match its definition value '%s'", returnedQ.name(), queueDefinition.name()));
        }
        if (!queueDefinition.topic().equals(returnedQ.topic()))
        {
            throw new QException(String.format("The returned queue topic '%s' does not match its definition value '%s'", returnedQ.topic(), queueDefinition.topic()));
        }
    }


    // Messages
    public static String checkQContentType(String value)
    {
        return check255("QContentType", value);
    }

    public static String checkQMessageId(String value)
    {
        return check127("QMessageId", value);
    }

    public static String checkQClaimant(String value)
    {
        return check127("QClaimant", value);
    }

    // Properties
    public static String checkPropertyName(String value)
    {
        return check450("QProperty Name", value);
    }

    public static String checkPropertyValue(String value)
    {
        return check450("QProperty Value", value);
    }

    public static Date checkPropertyDate(Date value)
    {
        return checkObjectTypeNotNull("QProperty Value", value);
    }

    public static String check450(final String objectType, final String value)
    {
        return checkMaxLength(450, objectType, value);
    }

    public static String check255(final String objectType, final String value)
    {
        return checkMaxLength(255, objectType, value);
    }

    public static String check127(final String objectType, final String value)
    {
        return checkMaxLength(127, objectType, value);
    }

    public static String checkMaxLength(final int max, final String objectType, final String value)
    {
        checkObjectTypeNotNull(objectType, value);
        checkArgument(value.length() <= max, String.format("%s must not be longer than %d in length", objectType, max));
        return value;
    }

    public static void checkArgument(boolean expression, @Nullable Object errorMessage)
    {
        try
        {
            Preconditions.checkArgument(expression, errorMessage);
        }
        catch (Exception e)
        {
            throw new QException(e.getMessage(), e);
        }
    }

    public static <T> T checkObjectTypeNotNull(final String objectType, final T value) {
        return checkNotNull(value, String.format("%s must not be null", objectType));
    }


    public static <T> T checkNotNull(T reference, @Nullable Object errorMessage) throws QException
    {
        try
        {
            return Preconditions.checkNotNull(reference, errorMessage);
        }
        catch (Exception e)
        {
            throw new QException(e.getMessage(), e);
        }
    }

    public static <T> T checkNotNull(T reference) throws QException
    {
        try
        {
            return Preconditions.checkNotNull(reference);
        }
        catch (Exception e)
        {
            throw new QException(e.getMessage(), e);
        }
    }


    public static void checkState(boolean state) throws QException
    {
        try
        {
            Preconditions.checkState(state);
        }
        catch (Exception e)
        {
            throw new QException(e.getMessage(), e);
        }
    }

    public static void checkState(boolean state, Object errorMsg) throws QException
    {
        try
        {
            Preconditions.checkState(state, errorMsg);
        }
        catch (Exception e)
        {
            throw new QException(e.getMessage(), e);
        }
    }

}

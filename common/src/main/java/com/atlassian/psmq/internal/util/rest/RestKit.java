package com.atlassian.psmq.internal.util.rest;

import com.atlassian.fugue.Option;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.util.Date;
import java.util.Locale;
import javax.annotation.Nonnull;

/**
 */
public class RestKit
{
    private static final DateTimeFormatter RFC1123_DATE_TIME_FORMATTER =
            DateTimeFormat.forPattern("EEE, dd MMM yyyy HH:mm:ss 'GMT'").withZone(DateTimeZone.UTC).withLocale(Locale.US);

    public static Option<Long> parseLong(final String s)
    {
        try
        {
            return Option.some(Long.parseLong(s));
        }
        catch (NumberFormatException e)
        {
            return Option.none();
        }
    }

    public static Option<Date> parseIsoDate(final String s)
    {
        Option<String> dt = Option.some(s);
        if (dt.isDefined())
        {
            return Option.some(toIsoDate(dt.get()));
        }
        return Option.none();
    }

    private static Date toIsoDate(String dt)
    {
        return ISODateTimeFormat.dateOptionalTimeParser().parseDateTime(dt).toDate();
    }


    public static boolean isModifiedSince(Option<String> lastModified, Date comparedTo)
    {
        if (lastModified.isDefined())
        {
            Date parsedDate = RFC1123_DATE_TIME_FORMATTER.parseDateTime(lastModified.get()).toDate();
            return comparedTo.after(parsedDate);
        }
        return false;
    }

    public static boolean ifNoneMatch(Option<String> eTag, @Nonnull Date comparedTo)
    {
        if (eTag.isDefined())
        {
            String dt = httpDate(comparedTo);
            return dt.equals(eTag.get());
        }
        return false;
    }

    public static String httpDate(Date date)
    {
        return date == null ? null : RFC1123_DATE_TIME_FORMATTER.print(date.getTime());
    }

    public static String isoDate(Date date)
    {
        return date == null ? null : ISODateTimeFormat.basicDateTime().print(date.getTime());
    }


}

package com.atlassian.psmq.internal.util;

import com.atlassian.fugue.Option;

import java.util.Optional;

/**
 * Some helpers that are NOT API
 */
public class HelperKit {

    /**
     * Most of the code was written using the much better fugue.Option but for API reasons
     * we use Java8 Optional.  So this allows us to be Option on the inside and Optional on the
     * outside.
     *
     * @param option the option to map to Optional
     * @param <T>    for two
     *
     * @return an Optional
     */
    public static <T> Optional<T> toOptional(Option<T> option) {
        return option.fold(() -> Optional.<T>empty(), v -> Optional.of(v));
    }

    /**
     * Optional to Option
     *
     * @param option the option to convert
     * @param <T>    for two
     *
     * @return an Option
     */
    public static <T> Option<T> toOption(Optional<T> option) {
        return Option.option(option.orElse(null));
    }
}

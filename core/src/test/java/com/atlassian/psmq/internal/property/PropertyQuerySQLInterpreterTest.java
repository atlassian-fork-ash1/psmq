package com.atlassian.psmq.internal.property;

import com.atlassian.psmq.api.property.query.NamedPropertyQueryPart;
import com.atlassian.psmq.api.property.query.QPropertyQuery;
import com.atlassian.psmq.api.property.query.QPropertyQueryBuilder;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.RelationalPathBase;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.psmq.api.property.query.QPropertyQueryBuilder.named;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class PropertyQuerySQLInterpreterTest {

    private static final StringPath NAME_COL = QTest.qTest.NAME_COL;
    private static final StringPath STRVAL_COL = QTest.qTest.STRVAL_COL;
    private static final NumberPath<Long> LONGVAL_COL = QTest.qTest.LONGVAL_COL;

    private PropertyQuerySQLInterpreter sqlInterpreter;

    @Before
    public void setUp() throws Exception {
        sqlInterpreter = new PropertyQuerySQLInterpreter(new PropertyQuerySQLInterpreter.PropertyColumnProvider() {
            @Override
            public StringPath getPropertyNameColumn() {
                return NAME_COL;
            }

            @Override
            public StringPath getPropertyStringValueColumn() {
                return STRVAL_COL;
            }

            @Override
            public NumberPath<Long> getPropertyLongValueColumn() {
                return LONGVAL_COL;
            }
        });
    }

    @Test
    public void test_basic_transform() throws Exception {
        QPropertyQuery propertyQuery = named("string1").contains("contains").and(named("long1").eq(666));

        String qdslExpected = NAME_COL.eq("string1").and(STRVAL_COL.contains("contains")).and(NAME_COL.eq("long1").and(LONGVAL_COL.eq(666L))).toString();

        String actual = sqlInterpreter.convert(propertyQuery).toString();

        assertThat(actual, equalTo(qdslExpected));

    }

    @Test
    public void test_and_composition() throws Exception {
        QPropertyQuery strContains = named("string1").contains("contains");
        QPropertyQuery long666 = named("long1").eq(666);
        QPropertyQuery isNotNull = named("notNull").isNotNull();

        QPropertyQuery propertyQuery = QPropertyQueryBuilder.and(strContains, long666).or(isNotNull);

        BooleanExpression qdslStrContains = NAME_COL.eq("string1").and(STRVAL_COL.contains("contains"));
        BooleanExpression qdslLong666 = NAME_COL.eq("long1").and(LONGVAL_COL.eq(666L));
        BooleanExpression qdslIsNotNull = NAME_COL.eq("notNull").isNotNull();

        String qdslExpected = new BooleanBuilder(qdslStrContains).and(qdslLong666).or(qdslIsNotNull).toString();

        String actual = sqlInterpreter.convert(propertyQuery).toString();

        assertThat(actual, equalTo(qdslExpected));
    }

    @Test
    public void test_or_composition_precedence() throws Exception {
        QPropertyQuery strContains = named("string1").contains("contains");
        QPropertyQuery long666 = named("long1").eq(666);
        QPropertyQuery isNotNull = named("notNull").isNotNull();

        QPropertyQuery propertyQuery = QPropertyQueryBuilder.or(strContains, long666).and(isNotNull);

        BooleanExpression qdslStrContains = NAME_COL.eq("string1").and(STRVAL_COL.contains("contains"));
        BooleanExpression qdslLong666 = NAME_COL.eq("long1").and(LONGVAL_COL.eq(666L));
        BooleanExpression qdslIsNotNull = NAME_COL.eq("notNull").isNotNull();

        String qdslExpected = new BooleanBuilder(qdslStrContains).or(qdslLong666).and(qdslIsNotNull).toString();

        String actual = sqlInterpreter.convert(propertyQuery).toString();

        assertThat(actual, equalTo(qdslExpected));
    }

    @Test
    public void test_method_mapping() throws Exception {
        long eq = 'e' + 'q';
        long goe = 'g' + 'o' + 'e';
        long gt = 'g' + 't';
        long loe = 'l' + 'o' + 'e';
        long lt = 'l' + 't';
        long ne = 'n' + 'e';

        NamedPropertyQueryPart prop = named("prop");
        QPropertyQuery query =
                // string
                prop.contains("contains")

                        .and(prop.startsWith("startsWith"))

                        .and(prop.endsWith("endsWith"))

                        .and(prop.like("like"))

                        // equality
                        .and(prop.eq("eq"))

                        .and(prop.eqIgnoreCase("eqIgnoreCase"))

                        .and(prop.ne("ne"))

                        .and(prop.ne(ne))

                        .and(prop.eq(eq))

                        // comparison
                        .and(prop.goe(goe))

                        .and(prop.gt(gt))

                        .and(prop.loe(loe))

                        .and(prop.lt(lt));

        BooleanExpression qdslProp = NAME_COL.eq("prop");

        String qdslExpected = new BooleanBuilder()

                .and(qdslProp.and(STRVAL_COL.contains("contains")))

                .and(qdslProp.and(STRVAL_COL.startsWith("startsWith")))

                .and(qdslProp.and(STRVAL_COL.endsWith("endsWith")))

                .and(qdslProp.and(STRVAL_COL.like("like")))

                .and(qdslProp.and(STRVAL_COL.eq("eq")))

                .and(qdslProp.and(STRVAL_COL.equalsIgnoreCase("eqIgnoreCase")))

                .and(qdslProp.and(STRVAL_COL.ne("ne")))

                .and(qdslProp.and(LONGVAL_COL.ne(ne)))

                .and(qdslProp.and(LONGVAL_COL.eq(eq)))

                .and(qdslProp.and(LONGVAL_COL.goe(goe)))

                .and(qdslProp.and(LONGVAL_COL.gt(gt)))

                .and(qdslProp.and(LONGVAL_COL.loe(loe)))

                .and(qdslProp.and(LONGVAL_COL.lt(lt)))

                .toString();

        String actual = sqlInterpreter.convert(query).toString();

        assertThat(actual, equalTo(qdslExpected));
    }

    @Test
    public void test_is_not_null_handling() throws Exception {
        NamedPropertyQueryPart prop = named("prop");
        QPropertyQuery query = prop.isNotNull();

        String qdslExpected = NAME_COL.eq("prop").isNotNull().toString();

        String actual = sqlInterpreter.convert(query).toString();

        assertThat(actual, equalTo(qdslExpected));

    }

    @Test
    public void test_is_null_handling() throws Exception {
        NamedPropertyQueryPart prop = named("prop");
        QPropertyQuery query = prop.isNull();

        String qdslExpected = NAME_COL.eq("prop").isNull().toString();

        String actual = sqlInterpreter.convert(query).toString();

        assertThat(actual, equalTo(qdslExpected));

    }

    static class QTest extends RelationalPathBase<QTest> {

        public static final QTest qTest = new QTest("test");

        public final StringPath NAME_COL = createString("NAME_COL");
        public final StringPath STRVAL_COL = createString("STRVAL_COL");
        public final NumberPath<Long> LONGVAL_COL = createNumber("LONGVAL_COL", Long.class);

        public QTest(String path) {
            super(QTest.class, PathMetadataFactory.forVariable(path), "PUBLIC", "TEST");
        }
    }
}
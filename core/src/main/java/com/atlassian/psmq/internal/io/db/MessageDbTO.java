package com.atlassian.psmq.internal.io.db;

import com.atlassian.fugue.Option;
import com.atlassian.psmq.api.message.QClaimant;
import com.atlassian.psmq.api.message.QContentType;
import com.atlassian.psmq.api.message.QContentVersion;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.message.QMessageBuilder;
import com.atlassian.psmq.api.message.QMessageId;
import com.atlassian.psmq.api.message.QMessagePriority;
import com.atlassian.psmq.api.message.QMessageSystemMetaData;
import com.atlassian.psmq.api.message.QSystemId;
import com.atlassian.psmq.api.property.QProperty;
import com.google.common.collect.Sets;

import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.psmq.internal.util.HelperKit.toOption;
import static com.atlassian.psmq.internal.util.HelperKit.toOptional;

/**
 */
public class MessageDbTO {
    private final long id;
    private final int version;
    private final int priority;
    private final int claimCount;
    private final Option<Long> claimTime;
    private final String messageId;
    private final String contentType;
    private final Option<Long> expiryTime;
    private final long createdTime;
    private final Option<String> msgData;
    private final Set<QProperty> properties;


    public MessageDbTO(final Long id, final String messageId, final Integer claimCount, final Long claimTime, final Long createdTime, final Integer version, final Integer priority, final String contentType, final Long expiryTime, final String msgData) {
        this.id = id;
        this.version = version;
        this.priority = priority;
        this.contentType = contentType;
        this.messageId = messageId;
        this.claimCount = claimCount;
        this.claimTime = Option.option(claimTime);
        this.expiryTime = Option.option(expiryTime);
        this.createdTime = createdTime;
        this.msgData = Option.option(msgData);
        this.properties = new HashSet<>();
    }

    public MessageDbTO(QMessage qMessage) {
        this.id = -1;
        this.claimCount = qMessage.systemMetaData().claimCount();
        this.claimTime = Option.none();
        this.version = qMessage.contentVersion().value();
        this.priority = qMessage.priority().value();
        this.contentType = qMessage.contentType().value();
        this.expiryTime = toOption(qMessage.expiryDate().map(Date::getTime));
        this.createdTime = qMessage.systemMetaData().createdDate().getTime();
        this.messageId = qMessage.messageId().value();
        this.msgData = Option.none();
        this.properties = Sets.newHashSet(qMessage.properties().asSet());
    }

    public String getContentType() {
        return contentType;
    }

    public Option<Long> getExpiryTime() {
        return expiryTime;
    }

    public int getClaimCount() {
        return claimCount;
    }

    public Option<Long> getClaimTime() {
        return claimTime;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public long getId() {
        return id;
    }

    public Option<String> getMsgData() {
        return msgData;
    }

    public int getVersion() {
        return version;
    }

    public int getPriority() {
        return priority;
    }

    public String getMessageId() {
        return messageId;
    }

    public Set<QProperty> getProperties() {
        return properties;
    }

    public void addProperty(QProperty property) {
        properties.add(property);
    }

    public QMessage toMessage(final QClaimant claimant) {
        SystemMetaData systemMetaData = new SystemMetaData(id, createdTime, claimant, claimCount);

        QMessageBuilder builder = QMessageBuilder.newMsg();
        return builder
                .withSystemMetaData(systemMetaData)
                .withContentType(new QContentType(this.getContentType()))
                .withContentVersion(new QContentVersion(this.getVersion()))
                .withExpiry(defaultedExpiry(this.getExpiryTime()))
                .withId(new QMessageId(this.getMessageId()))
                .withPriority(new QMessagePriority(this.priority))
                .withProperties(properties)
                .newBuffer()
                .append(toOptional(this.getMsgData()))
                .buildBuffer()
                .build();
    }

    private Optional<Date> defaultedExpiry(Option<Long> expiryTime) {
        return toOptional(expiryTime.map(Date::new));
    }

    static class SystemMetaData implements QMessageSystemMetaData {
        final QSystemId systemId;
        final Date created;
        final QClaimant claimant;
        final int claimCount;

        SystemMetaData(final long id, final long createdTime, final QClaimant claimant, final int claimCount) {
            this.systemId = new QSystemId(id);
            this.created = new Date(createdTime);
            this.claimant = claimant;
            this.claimCount = claimCount;
        }

        @Override
        public QSystemId systemId() {
            return systemId;
        }

        @Override
        public QClaimant claimant() {
            return claimant;
        }

        @Override
        public Date createdDate() {
            return created;
        }

        @Override
        public int claimCount() {
            return claimCount;
        }
    }
}

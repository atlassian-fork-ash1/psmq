package com.atlassian.psmq.internal.io;

import com.atlassian.fugue.Option;
import com.atlassian.psmq.api.QException;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.message.QMessageUpdate;
import com.atlassian.psmq.api.queue.QTopic;
import com.atlassian.psmq.api.queue.Queue;
import com.atlassian.psmq.api.queue.QueueQuery;
import com.atlassian.psmq.api.queue.QueueQueryBuilder;
import com.atlassian.psmq.internal.io.db.MessageDbTO;
import com.atlassian.psmq.internal.io.db.QueueDao;
import com.atlassian.psmq.internal.io.db.QueueDbTO;

import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

import static com.atlassian.psmq.api.internal.Validations.checkNotNull;
import static java.lang.String.format;

/**
 * This is responsible for writing messages to the database on behalf of a queue
 */
public class MessageWriter {
    private final QueueDao queueDao;

    public MessageWriter(final QueueDao queueDao) {
        this.queueDao = queueDao;
    }

    public long writeMessage(SessionInstructions instructions, QMessage message) throws QException {
        checkNotNull(instructions);
        checkNotNull(message);

        if (instructions.queue().isDefined()) {
            return writeToQueue(instructions, message);
        } else {
            return writeToQueuesAssociatedWithTopic(instructions, message);
        }
    }

    public void updateMessage(SessionInstructions instructions, QMessage message, QMessageUpdate messageUpdate) {
        checkNotNull(instructions);
        checkNotNull(message);

        String generalErrorMsg = format("Unable to update message '%s'", message.systemMetaData().systemId());

        TxnBoundary txn = instructions.txnBoundary();
        txn.run(generalErrorMsg, txnContext ->
        {
            queueDao.updateMsg(txnContext, message, messageUpdate);
            return true;
        });

    }

    private long writeToQueue(final SessionInstructions instructions, final QMessage message) {
        Queue queue = instructions.queue().get();
        String generalErrorMsg = format("Unable to write to queue '%s'", queue.name());

        TxnBoundary txn = instructions.txnBoundary();
        return txn.run(generalErrorMsg, txnContext ->
        {
            Option<QueueDbTO> queueDef = queueDao.getQueue(txnContext, queue.id().value());
            if (queueDef.isEmpty()) {
                throw new QException(format("Unable to find queue named '%s", queue.name()));
            }

            long qId = queueDef.get().getId();

            // one message written
            writeMsgToQueue(message, txnContext, qId);
            return 1L;
        });
    }

    private long writeToQueuesAssociatedWithTopic(final SessionInstructions instructions, final QMessage message) {
        QTopic topic = instructions.topic().get();
        String generalErrorMsg = format("Unable to write to topic '%s'", topic);

        TxnBoundary txn = instructions.txnBoundary();
        return txn.run(generalErrorMsg, txnContext ->
        {
            // find every queue associated with a topic
            QueueQuery qQuery = QueueQueryBuilder.newQuery().withTopic(topic).build();

            try (Stream<Long> queues = queueDao.queryQueueIds(txnContext, qQuery)) {
                AtomicLong i = new AtomicLong();
                //
                // write to each queue associated with that topic
                queues.forEach(qId -> {
                    writeMsgToQueue(message, txnContext, qId);
                    i.incrementAndGet();
                });

                // n messages written
                return i.longValue();
            }
        });
    }

    private Long writeMsgToQueue(final QMessage message, final TxnContext txnContext, final long qId) {
        Long msgId = queueDao.putMsg(txnContext, qId, new MessageDbTO(message), serialiseData(message));
        queueDao.heartBeatQueue(txnContext, qId);
        return msgId;
    }

    private Option<String> serialiseData(final QMessage message) {
        return Option.some(message.buffer().asString());
    }
}


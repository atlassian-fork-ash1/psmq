package com.atlassian.psmq.internal.io.db;

import com.atlassian.pocketknife.api.querydsl.DatabaseCompatibilityKit;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.atlassian.psmq.api.paging.LimitedPageRequest;
import com.atlassian.psmq.api.paging.PageResponse;
import com.atlassian.psmq.api.paging.PageResponseImpl;
import com.google.common.base.Predicates;
import com.querydsl.sql.dml.SQLInsertClause;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Helps with DB stuff
 */
@Component
public class DbKit {
    private final static AtomicReference<DatabaseCompatibilityKit> compatibilityKit = new AtomicReference<>();

    @Autowired
    public DbKit(DatabaseCompatibilityKit databaseCompatibilityKit) {
        compatibilityKit.set(databaseCompatibilityKit);
    }

    private static DatabaseCompatibilityKit databaseCompatibilityKit() {
        DatabaseCompatibilityKit databaseCompatibilityKit = DbKit.compatibilityKit.get();
        if (databaseCompatibilityKit == null) {
            throw new IllegalStateException("Accessed DatabaseCompatibilityKit too early");
        }
        return databaseCompatibilityKit;
    }

    /**
     * While we have to support HSQL 1.8 then we need this code.  We can move to native QueryDSL executeWithKey() once
     * this restriction is lifted.
     *
     * @param connection   the connection
     * @param insertClause the insert clause
     * @return the inserted key as a long
     */
    public static Long insertWithKey(DatabaseConnection connection, SQLInsertClause insertClause) {
        return databaseCompatibilityKit().executeWithKey(connection, insertClause, Long.class);
    }

    /**
     * You need to give us an list that is at least 1 bigger than the required number so that we can "detect"
     * hasMore or not.
     *
     * @param iterableOfOneExtra a list of results
     * @param page               the input page parameters
     * @param <T>                the domain type
     * @return a PageResponse of T
     */
    public static <T> PageResponse<T> page(final Stream<T> iterableOfOneExtra, LimitedPageRequest page) {
        List<T> list = iterableOfOneExtra.collect(Collectors.toList());
        return PageResponseImpl.filteredPageResponse(page, list, Predicates.alwaysTrue());
    }

}

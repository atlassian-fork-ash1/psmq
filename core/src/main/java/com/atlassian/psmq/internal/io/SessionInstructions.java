package com.atlassian.psmq.internal.io;

import com.atlassian.fugue.Option;
import com.atlassian.psmq.api.queue.QTopic;
import com.atlassian.psmq.internal.queue.QueueImpl;

import java.util.function.Supplier;

import static com.atlassian.psmq.api.internal.Validations.checkNotNull;

/**
 * This represents the some what stateful state of the QSession.
*/
public class SessionInstructions
{
    private final Supplier<TxnBoundary> txnBoundarySupplier;
    private final Option<QueueImpl> queue;
    private final Option<QTopic> topic;

    public SessionInstructions(final Supplier<TxnBoundary> txnBoundarySupplier, final QueueImpl queue)
    {
        this.txnBoundarySupplier = checkNotNull(txnBoundarySupplier);
        this.queue = Option.some(queue);
        this.topic = Option.none();
    }

    public SessionInstructions(final Supplier<TxnBoundary> txnBoundarySupplier, final QTopic topic)
    {
        this.txnBoundarySupplier = checkNotNull(txnBoundarySupplier);
        this.queue = Option.none();
        this.topic = Option.some(topic);
    }

    public SessionInstructions(final Supplier<TxnBoundary> txnBoundarySupplier)
    {
        this.txnBoundarySupplier = checkNotNull(txnBoundarySupplier);
        this.queue = Option.none();
        this.topic = Option.none();
    }


    public TxnBoundary txnBoundary()
    {
        return txnBoundarySupplier.get();
    }

    public Option<QueueImpl> queue()
    {
        return queue;
    }

    public Option<QTopic> topic()
    {
        return topic;
    }
}

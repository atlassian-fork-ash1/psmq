package com.atlassian.psmq.internal.io.db;

import com.atlassian.fugue.Option;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.atlassian.psmq.api.QException;
import com.atlassian.psmq.api.message.QClaimant;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.message.QMessagePriority;
import com.atlassian.psmq.api.message.QMessageQuery;
import com.atlassian.psmq.api.message.QMessageUpdate;
import com.atlassian.psmq.api.paging.PageResponse;
import com.atlassian.psmq.api.property.query.QPropertyQuery;
import com.atlassian.psmq.api.queue.QBrowseMessageQuery;
import com.atlassian.psmq.internal.io.TxnContext;
import com.atlassian.psmq.internal.property.PropertyQuerySQLInterpreter;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.SQLExpressions;
import com.querydsl.sql.dml.SQLInsertClause;
import com.querydsl.sql.dml.SQLUpdateClause;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.psmq.api.internal.Validations.checkNotNull;
import static com.atlassian.psmq.internal.io.querydsl.Tables.MESSAGE;
import static com.atlassian.psmq.internal.io.querydsl.Tables.MESSAGE_PROPERTY;
import static com.atlassian.psmq.internal.io.querydsl.Tables.QUEUE;
import static com.atlassian.psmq.internal.util.HelperKit.toOption;

/**
 * This is where the database magic happens for messages!  This was split from the QueueDao because it got too big
 */
@Component
public class MessageDao {
    private final PropertyDao propertyDao;

    @Autowired
    public MessageDao(final PropertyDao propertyDao) {
        this.propertyDao = propertyDao;
    }

    private MessageDbTO decorateWithMsgExtras(final TxnContext txnContext, MessageDbTO messageDbTO) {
        propertyDao.readMsgProperties(txnContext, messageDbTO);
        return messageDbTO;
    }

    public long getMessageCount(final TxnContext txnContext, final Long queueId, final QBrowseMessageQuery query) {
        checkNotNull(txnContext);

        Predicate whereClause = queryWhereClause(queueId, query);

        return txnContext.connection().select(MESSAGE.count()).from(MESSAGE).where(whereClause)
                .fetchCount();
    }

    public PageResponse<MessageDbTO> browse(final TxnContext txnContext, final long queueId, final QBrowseMessageQuery query) {
        checkNotNull(txnContext);

        DatabaseConnection connection = txnContext.connection();

        Predicate whereClause = queryWhereClause(queueId, query);

        List<Tuple> results = connection.select(MESSAGE.all()).from(MESSAGE)
                .where(whereClause)
                .offset(query.paging().getStart())
                .limit(query.paging().getLimit() + 1)
                .orderBy(MESSAGE.PRIORITY.desc(), MESSAGE.CREATED_TIME.asc(), MESSAGE.ID.asc())
                .distinct().fetch();
        return DbKit.page(results.stream().map(tuple -> decorateWithMsgExtras(txnContext, toMsgDef(tuple))), query.paging());
    }

    public Long putMsg(final TxnContext txnContext, final Long queueId, final MessageDbTO msg, final Option<String> msgData)
            throws QException {
        checkNotNull(txnContext);
        checkNotNull(queueId);
        checkNotNull(msg);
        checkNotNull(msgData);

        Long expiry = msg.getExpiryTime().getOrNull();
        long msgLength = msgData.map(String::length).getOrElse(0);

        long now = System.currentTimeMillis();
        incrementQueueMessageCount(txnContext, queueId);
        setQueueLastModified(txnContext, queueId);

        SQLInsertClause insertClause = txnContext.connection().insert(MESSAGE)
                //
                // when we write we have an empty claimant and no claim count
                .setNull(MESSAGE.CLAIMANT)
                .set(MESSAGE.CLAIMANT_TIME, now)
                .set(MESSAGE.CLAIM_COUNT, 0)

                .set(MESSAGE.CREATED_TIME, now)

                .set(MESSAGE.QUEUE_ID, queueId)
                .set(MESSAGE.MSG_ID, msg.getMessageId())
                .set(MESSAGE.CONTENT_TYPE, msg.getContentType())
                .set(MESSAGE.VERSION, msg.getVersion())
                .set(MESSAGE.PRIORITY, msg.getPriority())
                .set(MESSAGE.EXPIRY_TIME, expiry)
                .set(MESSAGE.MSG_LENGTH, msgLength)
                .set(MESSAGE.MSG_DATA, msgData.getOrNull());

        Long msgId = DbKit.insertWithKey(txnContext.connection(), insertClause);

        checkNotNull(msgId, "Unable to retrieve message system id for message put : " + msg.getMessageId());

        propertyDao.putMessageProperties(txnContext, msgId, msg.getProperties());


        return msgId;
    }

    public void incrementQueueMessageCount(final TxnContext txnContext, final long queueId) {
        incrementMessageCount(txnContext, queueId, 1);
    }

    public void incrementMessageCount(final TxnContext txnContext, final long queueId, final long value) {
        txnContext.connection()
                .update(QUEUE)
                .set(QUEUE.MESSAGE_COUNT, QUEUE.MESSAGE_COUNT.add(value))
                .where(QUEUE.ID.eq(queueId))
                .execute();
    }

    public Option<MessageDbTO> nextMsg(final TxnContext txnContext, final long queueId, final QMessageQuery query)
            throws QException {
        checkNotNull(txnContext);
        checkNotNull(queueId);

        // and select the message
        DatabaseConnection connection = txnContext.connection();

        Predicate whereClause = queryWhereClause(queueId, query);

        Option<Tuple> result = Option.option(connection.select(MESSAGE.all()).from(MESSAGE)
                .where(whereClause)
                // FIFO priority queue
                .orderBy(MESSAGE.PRIORITY.desc(), MESSAGE.CREATED_TIME.asc(), MESSAGE.ID.asc())
                .fetchFirst());

        if (result.isEmpty()) {
            return none();
        }

        Option<MessageDbTO> messageDbTO = result.map(this::toMsgDef);
        messageDbTO.foreach(msg -> propertyDao.readMsgProperties(txnContext, msg));
        return messageDbTO;
    }

    public boolean deleteMsg(final TxnContext txnContext, final long queueId, final long msgId) {
        checkNotNull(txnContext);

        DatabaseConnection connection = txnContext.connection();
        connection.delete(MESSAGE_PROPERTY).where(MESSAGE_PROPERTY.MESSAGE_ID.eq(msgId)).execute();
        long affectedRows = connection.delete(MESSAGE).where(MESSAGE.ID.eq(msgId)).execute();

        // The message has already been deleted by somebody else
        if (affectedRows == 0) {
            return false;
        }

        setQueueLastModified(txnContext, queueId);
        return true;
    }

    public void setQueueLastModified(final TxnContext txnContext, final long queueId) {
        long now = System.currentTimeMillis();
        // allow the update to do nothing if some one beat us - where beat us depends on Txn scope but hey
        txnContext.connection().update(QUEUE).set(QUEUE.MODIFIED_TIME, now)
                .where(QUEUE.ID.eq(queueId)
                        .and(QUEUE.MODIFIED_TIME.lt(now)))
                .execute();
    }

    private Predicate queryWhereClause(long queueId, QMessageQuery query) {
        BooleanExpression whereClause = MESSAGE.QUEUE_ID.eq(queueId);
        //
        // only read unclaimed messages
        whereClause = whereClause.and(MESSAGE.CLAIMANT.isNull());

        if (query.contentType().isPresent()) {
            whereClause = whereClause.and(MESSAGE.CONTENT_TYPE.eq(query.contentType().get().value()));
        }
        if (query.contentVersion().isPresent()) {
            whereClause = whereClause.and(MESSAGE.VERSION.eq(query.contentVersion().get().value()));
        }
        if (query.priority().isPresent()) {
            whereClause = whereClause.and(mapPriority(query.priority().get()));
        }
        if (query.messageId().isPresent()) {
            whereClause = whereClause.and(MESSAGE.MSG_ID.eq(query.messageId().get().value()));
        }
        if (query.properties().isPresent()) {
            Predicate propertiesPredicate = interpret(query.properties().get());
            BooleanExpression propertyExists = SQLExpressions.selectOne().from(MESSAGE_PROPERTY).where(MESSAGE_PROPERTY.MESSAGE_ID.eq(MESSAGE.ID).and(propertiesPredicate)).exists();
            whereClause = whereClause.and(propertyExists);
        }
        return whereClause;
    }

    private Predicate interpret(final QPropertyQuery propertyQuery) {
        return interpreter().convert(propertyQuery);
    }

    private PropertyQuerySQLInterpreter interpreter() {
        return new PropertyQuerySQLInterpreter(new PropertyQuerySQLInterpreter.PropertyColumnProvider() {
            @Override
            public StringPath getPropertyNameColumn() {
                return MESSAGE_PROPERTY.NAME;
            }

            @Override
            public StringPath getPropertyStringValueColumn() {
                return MESSAGE_PROPERTY.STRING_VALUE;
            }

            @Override
            public NumberPath<Long> getPropertyLongValueColumn() {
                return MESSAGE_PROPERTY.LONG_VALUE;
            }
        });
    }

    public boolean claimMsg(final TxnContext txnContext, final long queueId, final long msgId) {
        QClaimant claimant = checkNotNull(txnContext.claimant());
        long affectedRows = txnContext.connection()
                .update(MESSAGE)
                .set(MESSAGE.CLAIMANT, claimant.value())
                .set(MESSAGE.CLAIM_COUNT, MESSAGE.CLAIM_COUNT.add(1))
                .where(MESSAGE.ID.eq(msgId)
                        .and(MESSAGE.QUEUE_ID.eq(queueId))
                        .and(MESSAGE.CLAIMANT.isNull()))
                .execute();

        if (affectedRows == 0) {
            return false;
        }

        return decrementQueueMessageCount(txnContext, queueId);
    }

    public boolean decrementQueueMessageCount(final TxnContext txnContext, final long queueId) {
        long affectedRows = txnContext.connection()
                .update(QUEUE)
                .set(QUEUE.MESSAGE_COUNT, QUEUE.MESSAGE_COUNT.subtract(1))
                .where(QUEUE.ID.eq(queueId).and(QUEUE.MESSAGE_COUNT.gt(0L)))
                .execute();

        return affectedRows == 1;
    }

    public boolean unresolveMsg(final TxnContext txnContext, final long queueId, final long msgId) {
        QClaimant claimant = checkNotNull(txnContext.claimant());

        long affectedRows = txnContext.connection()
                .update(MESSAGE)
                .setNull(MESSAGE.CLAIMANT)
                .setNull(MESSAGE.CLAIMANT_TIME)
                .where(MESSAGE.ID.eq(msgId)
                        .and(MESSAGE.QUEUE_ID.eq(queueId))
                        .and(MESSAGE.CLAIMANT.eq(claimant.value())))
                .execute();

        if (affectedRows == 0) {
            // We're either trying to:
            // - unresolve a message twice
            // - unresolve a deleted message
            //
            // Either way, we don't need to update the message count
            return false;
        }

        incrementQueueMessageCount(txnContext, queueId);
        return true;
    }

    public long unresolveAllClaimedMessages(final TxnContext txnContext, final long queueId) {
        QClaimant claimant = checkNotNull(txnContext.claimant());

        long affectedRows = txnContext.connection()
                .update(MESSAGE)
                .setNull(MESSAGE.CLAIMANT)
                .setNull(MESSAGE.CLAIMANT_TIME)
                .where(MESSAGE.QUEUE_ID.eq(queueId)
                        .and(MESSAGE.CLAIMANT.eq(claimant.value())))
                .execute();

        incrementMessageCount(txnContext, queueId, affectedRows);
        return affectedRows;
    }

    public long unresolveAllClaimedMessages(final TxnContext txnContext) {
        List<Long> queueIds = txnContext.connection()
                .select(QUEUE.ID)
                .from(QUEUE)
                .fetch();

        long affectedRows = 0;
        for (long id : queueIds) {
            affectedRows += unresolveAllClaimedMessages(txnContext, id);
        }

        return affectedRows;
    }

    private MessageDbTO toMsgDef(final Tuple tuple) {
        return new MessageDbTO(
                tuple.get(MESSAGE.ID),
                tuple.get(MESSAGE.MSG_ID),

                tuple.get(MESSAGE.CLAIM_COUNT),
                tuple.get(MESSAGE.CLAIMANT_TIME),
                tuple.get(MESSAGE.CREATED_TIME),

                tuple.get(MESSAGE.VERSION),
                tuple.get(MESSAGE.PRIORITY),
                tuple.get(MESSAGE.CONTENT_TYPE),
                tuple.get(MESSAGE.EXPIRY_TIME),
                tuple.get(MESSAGE.MSG_DATA)
        );
    }

    private BooleanExpression mapPriority(final QMessagePriority p) {
        int value = p.value();
        QMessagePriority.Comparison comparison = p.comparison();
        if (comparison.equals(QMessagePriority.Comparison.GREATER_THAN)) {
            return MESSAGE.PRIORITY.gt(value);
        } else if (comparison.equals(QMessagePriority.Comparison.GREATER_THAN_OR_EQUAL_TO)) {
            return MESSAGE.PRIORITY.goe(value);
        } else if (comparison.equals(QMessagePriority.Comparison.EQUAL_TO)) {
            return MESSAGE.PRIORITY.eq(value);
        } else if (comparison.equals(QMessagePriority.Comparison.LESS_THAN_OR_EQUAL_TO)) {
            return MESSAGE.PRIORITY.loe(value);
        } else {
            return MESSAGE.PRIORITY.lt(value);
        }
    }

    public void updateMsg(TxnContext txnContext, QMessage message, QMessageUpdate messageUpdate) {
        checkNotNull(txnContext);
        checkNotNull(message);
        checkNotNull(messageUpdate);

        long msgId = message.systemMetaData().systemId().value();
        SQLUpdateClause updateClause = txnContext.connection().update(MESSAGE)
                .where(MESSAGE.ID.eq(msgId));

        toOption(messageUpdate.expiryDate()).forEach(expiry -> updateClause.set(MESSAGE.EXPIRY_TIME, expiry.getTime()));
        toOption(messageUpdate.priority()).forEach(priority -> updateClause.set(MESSAGE.PRIORITY, priority.value()));

        updateClause.execute();

        if (messageUpdate.properties().isPresent()) {
            propertyDao.deleteMessageProperties(txnContext, msgId);
            propertyDao.putMessageProperties(txnContext, msgId, messageUpdate.properties().get());
        }
    }
}

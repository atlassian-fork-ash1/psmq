package com.atlassian.psmq.internal.io;

import com.atlassian.psmq.internal.io.db.QueueDao;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 */
@Component
public class MessageWriterFactory implements FactoryBean
{
    private final QueueDao queueDao;

    @Autowired
    public MessageWriterFactory(final QueueDao queueDao)
    {
        this.queueDao = queueDao;
    }

    @Override
    public Object getObject() throws Exception
    {
        return new MessageWriter(queueDao);
    }

    @Override
    public Class getObjectType()
    {
        return MessageWriter.class;
    }

    @Override
    public boolean isSingleton()
    {
        return true;
    }
}

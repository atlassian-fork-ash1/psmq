package com.atlassian.psmq.internal.io;

import com.atlassian.psmq.internal.io.db.QueueDao;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 */
@Component
public class MessageReaderFactory implements FactoryBean
{
    private final QueueDao queueDao;

    @Autowired
    public MessageReaderFactory(final QueueDao queueDao)
    {
        this.queueDao = queueDao;
    }

    @Override
    public Object getObject() throws Exception
    {
        return new MessageReader(queueDao);
    }

    @Override
    public Class getObjectType()
    {
        return MessageReader.class;
    }

    @Override
    public boolean isSingleton()
    {
        return true;
    }
}

package com.atlassian.psmq.internal.io;

import com.atlassian.pocketknife.api.querydsl.DatabaseConnectionConverter;

/**
 * A Txn boundary that should be used when we have external transactions
 *
 * At this stage is the same as NoCommit but keeping this class for clearer naming
 */
public class TxnBoundarySessionExternal extends TxnBoundarySession
{
    public TxnBoundarySessionExternal(final TxnFixture txnFixture, final DatabaseConnectionConverter databaseConnectionConverter)
    {
        super(txnFixture, databaseConnectionConverter);
    }
}

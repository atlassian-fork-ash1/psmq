package com.atlassian.psmq.internal.io.db;

import com.atlassian.psmq.api.property.QProperty;
import com.atlassian.psmq.api.property.QPropertyBuilder;
import com.atlassian.psmq.internal.io.TxnContext;
import com.querydsl.core.Tuple;
import com.querydsl.sql.dml.SQLInsertClause;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Set;

import static com.atlassian.psmq.api.internal.Validations.checkNotNull;
import static com.atlassian.psmq.internal.io.querydsl.Tables.MESSAGE_PROPERTY;
import static com.atlassian.psmq.internal.io.querydsl.Tables.QUEUE_PROPERTY;

/**
 * DB read and writes for object properties
 */
@Component
public class PropertyDao {
    static final long TRUE_LONG = 1;
    static final long FALSE_LONG = 0;

    public void readMsgProperties(final TxnContext txnContext, final MessageDbTO msg) {
        List<Tuple> tuples = txnContext.connection()
                .select(MESSAGE_PROPERTY.all())
                .from(MESSAGE_PROPERTY)
                .where(MESSAGE_PROPERTY.MESSAGE_ID.eq(msg.getId()))
                .fetch();

        tuples.stream()
                .map(this::makeMessageProperty)
                .forEach(msg::addProperty);
    }

    private QProperty makeMessageProperty(final Tuple tuple) {
        return makeProperty(
                tuple.get(MESSAGE_PROPERTY.NAME),
                tuple.get(MESSAGE_PROPERTY.PROPERTY_TYPE),
                tuple.get(MESSAGE_PROPERTY.STRING_VALUE),
                tuple.get(MESSAGE_PROPERTY.LONG_VALUE)
        );
    }

    public long putMessageProperties(final TxnContext txnContext, final Long msgId, final Set<QProperty> properties) {
        if (properties.size() > 0) {
            SQLInsertClause insert = txnContext.connection().insert(MESSAGE_PROPERTY);
            for (QProperty property : properties) {
                insert
                        .set(MESSAGE_PROPERTY.MESSAGE_ID, msgId)
                        .set(MESSAGE_PROPERTY.NAME, property.name())
                        .set(MESSAGE_PROPERTY.PROPERTY_TYPE, propertyTypeStorageFmt(property));
                if (property.type() == QProperty.Type.STRING) {
                    insert.set(MESSAGE_PROPERTY.STRING_VALUE, property.stringValue());
                } else {
                    //noinspection unchecked
                    insert.set(MESSAGE_PROPERTY.LONG_VALUE, getLongVal(property));
                }
                insert = insert.addBatch();
            }
            return insert.execute();
        }
        return 0;
    }

    public void readQueueProperties(final TxnContext txnContext, final QueueDbTO queue) {
        List<Tuple> tuples = txnContext.connection()
                .select(QUEUE_PROPERTY.all())
                .from(QUEUE_PROPERTY)
                .where(QUEUE_PROPERTY.QUEUE_ID.eq(queue.getId()))
                .fetch();

        tuples.stream()
                .map(this::makeQueueProperty)
                .forEach(queue::addProperty);
    }

    private QProperty makeQueueProperty(final Tuple tuple) {
        return makeProperty(
                tuple.get(QUEUE_PROPERTY.NAME),
                tuple.get(QUEUE_PROPERTY.PROPERTY_TYPE),
                tuple.get(QUEUE_PROPERTY.STRING_VALUE),
                tuple.get(QUEUE_PROPERTY.LONG_VALUE)
        );
    }

    public long putQueueProperties(final TxnContext txnContext, final Long queueId, Set<QProperty> properties) {
        if (properties.size() > 0) {
            SQLInsertClause insert = txnContext.connection().insert(QUEUE_PROPERTY);
            for (QProperty property : properties) {
                insert
                        .set(QUEUE_PROPERTY.QUEUE_ID, queueId)
                        .set(QUEUE_PROPERTY.NAME, property.name())
                        .set(QUEUE_PROPERTY.PROPERTY_TYPE, propertyTypeStorageFmt(property));
                if (property.type() == QProperty.Type.STRING) {
                    insert.set(QUEUE_PROPERTY.STRING_VALUE, property.stringValue());
                } else {
                    //noinspection unchecked
                    insert.set(QUEUE_PROPERTY.LONG_VALUE, getLongVal(property));
                }
                insert = insert.addBatch();
            }
            return insert.execute();
        }
        return 0;
    }

    public void deleteQueueProperties(final TxnContext txnContext, long queueId) {
        txnContext.connection().delete(QUEUE_PROPERTY)
                .where(QUEUE_PROPERTY.QUEUE_ID.eq(queueId))
                .execute();
    }

    public void deleteMessageProperties(final TxnContext txnContext, long msgId) {
        txnContext.connection().delete(MESSAGE_PROPERTY)
                .where(MESSAGE_PROPERTY.MESSAGE_ID.eq(msgId))
                .execute();
    }

    private long getLongVal(final QProperty property) {
        long longValue = 0;
        if (property.type() == QProperty.Type.LONG) {
            longValue = property.longValue();
        } else if (property.type() == QProperty.Type.BOOLEAN) {
            longValue = property.booleanValue() ? TRUE_LONG : FALSE_LONG;
        } else if (property.type() == QProperty.Type.DATE) {
            longValue = property.dateValue().getTime();
        }
        return longValue;
    }

    private String propertyTypeStorageFmt(final QProperty property) {
        String type = "S";
        switch (property.type()) {
            case BOOLEAN:
                type = "B";
                break;
            case DATE:
                type = "D";
                break;
            case LONG:
                type = "L";
                break;
            case STRING:
                type = "S";
                break;
        }
        return type;
    }

    private QProperty makeProperty(
            final String name,
            final String type,
            final String stringValue,
            final Long longValue) {
        QPropertyBuilder builder = QPropertyBuilder.newProperty();

        if ("S".equals(type)) {
            return builder.with(name, stringValue).build();
        } else {
            checkNotNull(longValue, "Long value must not be null for property type '" + type + "'");
            if ("L".equals(type)) {
                return builder.with(name, longValue).build();
            } else if ("B".equals(type)) {
                return builder.with(name, TRUE_LONG == longValue).build();
            } else if ("D".equals(type)) {
                return builder.with(name, new Date(longValue)).build();
            }
        }
        return builder.with(name, stringValue).build();
    }

}


package com.atlassian.psmq.internal.io.db;

import com.atlassian.fugue.Option;
import com.atlassian.psmq.api.property.QProperty;
import com.atlassian.psmq.api.queue.QTopic;
import com.atlassian.psmq.api.queue.Queue;
import com.atlassian.psmq.internal.queue.QueueImpl;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static com.atlassian.psmq.api.internal.Validations.checkNotNull;

/**
 */
@SuppressWarnings ("UnusedDeclaration")
public class QueueDbTO
{
    private final long id;
    private final String name;
    private final String purpose;
    private final Option<QTopic> topic;
    private final Date created;
    private final Date lastModified;
    private final long approxMsgCount;
    private final Set<QProperty> properties;

    public QueueDbTO(final Long id, final String name, final String purpose, final Option<QTopic> topic, final Long created, final Long lastModified, final long approxMsgCount)
    {
        this.id = id;
        this.name = name;
        this.purpose = purpose;
        this.topic = topic;
        this.created = new Date(created);
        this.lastModified = new Date(lastModified);
        this.approxMsgCount = approxMsgCount;
        this.properties = new HashSet<>();
    }

    public QueueDbTO(final Long id, final QueueDbTO queueDbTO, final Long created, final Long lastModified, final long approxMsgCount)
    {
        this.id = id;
        this.name = queueDbTO.name;
        this.purpose = queueDbTO.purpose;
        this.topic = queueDbTO.topic;
        this.created = new Date(created);
        this.lastModified = new Date(lastModified);
        this.approxMsgCount = approxMsgCount;
        this.properties = new HashSet<>(checkNotNull(queueDbTO.getProperties()));
    }

    public QueueDbTO(final String name, final String purpose, final Option<QTopic> topic, final Set<QProperty> properties)
    {
        this.topic = topic;
        Date now = new Date();
        this.id = Long.MIN_VALUE;
        this.name = name;
        this.purpose = purpose;
        this.created = now;
        this.lastModified = now;
        this.approxMsgCount = 0;
        this.properties = new HashSet<>(checkNotNull(properties));
    }

    public long getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public String getPurpose()
    {
        return purpose;
    }

    public Option<QTopic> getTopic()
    {
        return topic;
    }

    public long getApproxMsgCount()
    {
        return approxMsgCount;
    }

    public Date getCreated()
    {
        return created;
    }

    public Date getLastModified()
    {
        return lastModified;
    }

    public Set<QProperty> getProperties()
    {
        return properties;
    }

    public void addProperty(QProperty property)
    {
        properties.add(property);
    }

    public Queue toQ()
    {
        return new QueueImpl(id, name, purpose, topic, created, lastModified, approxMsgCount, properties);
    }
}

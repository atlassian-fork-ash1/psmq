package com.atlassian.psmq.internal.visiblefortesting;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.querydsl.sql.SQLExpressions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.atlassian.psmq.internal.io.querydsl.Tables.MESSAGE;
import static com.atlassian.psmq.internal.io.querydsl.Tables.MESSAGE_PROPERTY;
import static com.atlassian.psmq.internal.io.querydsl.Tables.QUEUE;
import static com.atlassian.psmq.internal.io.querydsl.Tables.QUEUE_PROPERTY;

/**
 */
@ExportAsService
@Component
public class QueueResetterImpl implements QueueResetter {
    private final DatabaseAccessor databaseAccessor;

    @Autowired
    public QueueResetterImpl(DatabaseAccessor databaseAccessor1) {
        this.databaseAccessor = databaseAccessor1;
    }

    /**
     * Used for testing to reset EVERYTHING between tests
     */
    @Override
    public void eraseQs() {
        databaseAccessor.runInTransaction(dbConnection -> {
            dbConnection.delete(MESSAGE_PROPERTY).execute();
            dbConnection.delete(QUEUE_PROPERTY).execute();
            dbConnection.delete(MESSAGE).execute();
            dbConnection.delete(QUEUE).execute();
            return true;
        });
    }

    @Override
    public void eraseQ(final long queueId) {
        databaseAccessor.runInTransaction(dbConnection -> {
            dbConnection.delete(MESSAGE_PROPERTY)
                    .where(MESSAGE_PROPERTY.MESSAGE_ID.in(
                            SQLExpressions.select(MESSAGE.ID).from(MESSAGE)
                                    .where(MESSAGE.QUEUE_ID.eq(queueId))
                    ))
                    .execute();

            dbConnection.delete(QUEUE_PROPERTY)
                    .where(QUEUE_PROPERTY.QUEUE_ID.eq(queueId)).execute();

            dbConnection.delete(MESSAGE)
                    .where(MESSAGE.QUEUE_ID.eq(queueId)).execute();

            dbConnection.delete(QUEUE)
                    .where(QUEUE.ID.eq(queueId)).execute();

            return true;
        });
    }
}

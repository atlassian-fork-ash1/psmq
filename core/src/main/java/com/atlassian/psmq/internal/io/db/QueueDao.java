package com.atlassian.psmq.internal.io.db;

import com.atlassian.fugue.Option;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.atlassian.psmq.api.QException;
import com.atlassian.psmq.api.message.QClaimant;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.message.QMessageQuery;
import com.atlassian.psmq.api.message.QMessageUpdate;
import com.atlassian.psmq.api.paging.PageResponse;
import com.atlassian.psmq.api.property.query.QPropertyQuery;
import com.atlassian.psmq.api.queue.QBrowseMessageQuery;
import com.atlassian.psmq.api.queue.QBrowseMessageQueryBuilder;
import com.atlassian.psmq.api.queue.QTopic;
import com.atlassian.psmq.api.queue.QueueQuery;
import com.atlassian.psmq.api.queue.QueueUpdate;
import com.atlassian.psmq.internal.io.TxnContext;
import com.atlassian.psmq.internal.property.PropertyQuerySQLInterpreter;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.SQLExpressions;
import com.querydsl.sql.SQLQuery;
import com.querydsl.sql.dml.SQLInsertClause;
import com.querydsl.sql.dml.SQLUpdateClause;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Stream;

import static com.atlassian.fugue.Option.option;
import static com.atlassian.psmq.api.internal.Validations.checkNotNull;
import static com.atlassian.psmq.internal.io.querydsl.Tables.MESSAGE;
import static com.atlassian.psmq.internal.io.querydsl.Tables.MESSAGE_PROPERTY;
import static com.atlassian.psmq.internal.io.querydsl.Tables.QUEUE;
import static com.atlassian.psmq.internal.io.querydsl.Tables.QUEUE_PROPERTY;
import static com.atlassian.psmq.internal.util.HelperKit.toOption;

/**
 * This is where the database magic happens!
 */
@Component
public class QueueDao {
    private final MessageDao messageDao;
    private final PropertyDao propertyDao;

    @Autowired
    public QueueDao(final MessageDao messageDao, final PropertyDao propertyDao) {
        this.messageDao = messageDao;
        this.propertyDao = propertyDao;
    }


    public Option<QueueDbTO> getQueue(final TxnContext txnContext, final long queueId) {
        checkNotNull(txnContext);

        Option<Tuple> select = option(txnContext.connection().select(QUEUE.all()).from(QUEUE)
                .where(QUEUE.ID.eq(queueId))
                .fetchFirst());
        return select
                .map(tuple -> decorateWithQExtras(txnContext, tuple));
    }

    public Option<QueueDbTO> selectQueue(final TxnContext txnContext, final QueueDbTO queueDbTO) throws QException {
        checkNotNull(txnContext);
        checkNotNull(queueDbTO);

        Option<Tuple> select = option(txnContext.connection().select(QUEUE.all()).from(QUEUE)
                .where(QUEUE.NAME.eq(queueDbTO.getName()))
                .fetchFirst());
        return select
                .map(tuple -> decorateWithQExtras(txnContext, tuple));
    }

    public long exclusiveAccess(final TxnContext txnContext, final QueueDbTO queueDbTO) {
        checkNotNull(txnContext);
        checkNotNull(queueDbTO);

        long now = System.currentTimeMillis();
        long heartBeatThen = now - txnContext.claimantHeartBeatMillis();

        BooleanExpression expiredNonNullClaimant = QUEUE.CLAIMANT.isNotNull().and(QUEUE.CLAIMANT_TIME.lt(heartBeatThen));

        BooleanExpression nullClaimantOrExpiredClaimant = QUEUE.CLAIMANT.isNull().or(expiredNonNullClaimant);

        BooleanExpression whereClause = QUEUE.ID.eq(queueDbTO.getId()).and(nullClaimantOrExpiredClaimant);

        return txnContext.connection().update(QUEUE)
                .set(QUEUE.CLAIMANT, txnContext.claimant().value())
                .set(QUEUE.CLAIMANT_TIME, now)
                .where(whereClause)
                .execute();
    }

    private QueueDbTO decorateWithQExtras(final TxnContext txnContext, Tuple queueTuple) {
        long msgCount = getMessageCount(txnContext, queueTuple.get(QUEUE.ID), QBrowseMessageQueryBuilder.anyMessage());
        QueueDbTO queueDbTO = toQueueDbTO(queueTuple, msgCount);

        propertyDao.readQueueProperties(txnContext, queueDbTO);
        return queueDbTO;
    }

    public QueueDbTO createQueue(final TxnContext txnContext, final QueueDbTO queueDbTO) {
        checkNotNull(txnContext);
        checkNotNull(queueDbTO);

        long now = System.currentTimeMillis();
        String topic = queueDbTO.getTopic().map(QTopic::value).getOrNull();
        SQLInsertClause insertClause = txnContext.connection().insert(QUEUE)
                .set(QUEUE.NAME, queueDbTO.getName())
                .set(QUEUE.PURPOSE, queueDbTO.getPurpose())
                .set(QUEUE.MESSAGE_COUNT, 0L)
                .set(QUEUE.TOPIC, topic)
                .set(QUEUE.CREATED_TIME, now)
                .set(QUEUE.MODIFIED_TIME, now);

        Long qId = DbKit.insertWithKey(txnContext.connection(), insertClause);
        checkNotNull(qId, "Unable to retrieve queue system id for queue create : " + queueDbTO.getName());

        propertyDao.putQueueProperties(txnContext, qId, queueDbTO.getProperties());

        return new QueueDbTO(qId, queueDbTO, now, now, 0L);
    }

    public long updateQueue(final TxnContext txnContext, final long queueId, final QueueUpdate queueUpdate) {
        checkNotNull(txnContext);
        checkNotNull(queueUpdate);

        long now = System.currentTimeMillis();

        SQLUpdateClause updateClause = txnContext.connection().update(QUEUE)
                .set(QUEUE.MODIFIED_TIME, now)
                .where(QUEUE.ID.eq(queueId));

        toOption(queueUpdate.purpose()).forEach(s -> updateClause.set(QUEUE.PURPOSE, s));
        toOption(queueUpdate.topic()).forEach(t -> updateClause.set(QUEUE.TOPIC, t.value()));

        long affected = updateClause.execute();

        if (queueUpdate.properties().isPresent()) {
            propertyDao.deleteQueueProperties(txnContext, queueId);
            propertyDao.putQueueProperties(txnContext, queueId, queueUpdate.properties().get());
        }
        return affected;
    }

    public void updateMsg(TxnContext txnContext, QMessage message, QMessageUpdate messageUpdate) {
        messageDao.updateMsg(txnContext, message, messageUpdate);
    }

    public PageResponse<QueueDbTO> queryQueues(final TxnContext txnContext, final QueueQuery queueQuery) {
        checkNotNull(txnContext);

        DatabaseConnection connection = txnContext.connection();

        Predicate whereClause = buildQQueryWhereClause(queueQuery);
        SQLQuery<Tuple> select = connection.select(QUEUE.all()).from(QUEUE).where(whereClause).offset(queueQuery.paging().getStart()).limit(queueQuery.paging().getLimit() + 1).orderBy(QUEUE.CREATED_TIME.desc(), QUEUE.ID.asc()).distinct();

        Stream<QueueDbTO> queueDbTOStream = select.fetch().stream().map(tuple -> decorateWithQExtras(txnContext, tuple));
        return DbKit.page(queueDbTOStream, queueQuery.paging());
    }


    public Stream<Long> queryQueueIds(final TxnContext txnContext, final QueueQuery queueQuery) {
        checkNotNull(txnContext);

        List<Long> results = txnContext.connection()
                .select(QUEUE.ID).from(QUEUE)
                .where(buildQQueryWhereClause(queueQuery))
                .orderBy(QUEUE.CREATED_TIME.desc()).fetch();
        return results.stream();
    }

    private Predicate buildQQueryWhereClause(final QueueQuery query) {
        BooleanBuilder whereClause = new BooleanBuilder();
        if (query.queueId().isPresent()) {
            long qId = query.queueId().get().value();
            whereClause = whereClause.and(QUEUE.ID.eq(qId));
        }
        if (query.queueName().isPresent()) {
            whereClause = whereClause.and(QUEUE.NAME.eq(query.queueName().get()));
        }
        if (query.topic().isPresent()) {
            whereClause = whereClause.and(QUEUE.TOPIC.eq(query.topic().get().value()));
        }
        if (query.properties().isPresent()) {
            Predicate propertiesPredicate = interpret(query.properties().get());
            BooleanExpression propertyExists = SQLExpressions.selectOne().from(QUEUE_PROPERTY).where(QUEUE_PROPERTY.QUEUE_ID.eq(QUEUE.ID).and(propertiesPredicate)).exists();
            whereClause = whereClause.and(propertyExists);
        }
        return whereClause;
    }

    private Predicate interpret(final QPropertyQuery propertyQuery) {
        return interpreter().convert(propertyQuery);
    }

    private PropertyQuerySQLInterpreter interpreter() {
        return new PropertyQuerySQLInterpreter(new PropertyQuerySQLInterpreter.PropertyColumnProvider() {
            @Override
            public StringPath getPropertyNameColumn() {
                return QUEUE_PROPERTY.NAME;
            }

            @Override
            public StringPath getPropertyStringValueColumn() {
                return QUEUE_PROPERTY.STRING_VALUE;
            }

            @Override
            public NumberPath<Long> getPropertyLongValueColumn() {
                return QUEUE_PROPERTY.LONG_VALUE;
            }
        });
    }

    public long getMessageCount(final TxnContext txnContext, final Long queueId, final QBrowseMessageQuery query) {
        return messageDao.getMessageCount(txnContext, queueId, query);
    }

    public PageResponse<MessageDbTO> browse(final TxnContext txnContext, final long queueId, final QBrowseMessageQuery query) {
        return messageDao.browse(txnContext, queueId, query);
    }

    public boolean deleteQueue(final TxnContext txnContext, final long queueId) throws QException {
        return purgeAndDeleteQ(true, txnContext, queueId);
    }

    public boolean purgeQueue(final TxnContext txnContext, final long queueId) {
        return purgeAndDeleteQ(false, txnContext, queueId);
    }

    private boolean purgeAndDeleteQ(final boolean deleteQ, final TxnContext txnContext, final long queueId) {
        checkNotNull(txnContext);

        DatabaseConnection connection = txnContext.connection();
        //
        // reset message count
        connection.update(QUEUE)
                .set(QUEUE.MESSAGE_COUNT, 0L)
                .where(QUEUE.ID.eq(queueId))
                .execute();

        //
        // message properties
        connection.delete(MESSAGE_PROPERTY)
                .where(MESSAGE_PROPERTY.MESSAGE_ID.in(
                        SQLExpressions.select(MESSAGE.ID)
                                .from(MESSAGE)
                                .where(MESSAGE.QUEUE_ID.eq(queueId))
                ))
                .execute();

        //
        // messages
        connection.delete(MESSAGE)
                .where(MESSAGE.QUEUE_ID.eq(queueId))
                .execute();
        if (deleteQ) {
            // queue properties
            connection.delete(QUEUE_PROPERTY).where(QUEUE_PROPERTY.QUEUE_ID.eq(queueId))
                    .execute();
            //
            // the queue
            connection.delete(QUEUE).where(QUEUE.ID.eq(queueId))
                    .execute();
        } else {
            messageDao.setQueueLastModified(txnContext, queueId);
        }
        return true;
    }

    public Long putMsg(final TxnContext txnContext, final Long queueId, final MessageDbTO msg, final Option<String> msgData)
            throws QException {
        return messageDao.putMsg(txnContext, queueId, msg, msgData);
    }

    public Option<MessageDbTO> nextMsg(final TxnContext txnContext, final long queueId, final QMessageQuery query)
            throws QException {
        return messageDao.nextMsg(txnContext, queueId, query);
    }

    public boolean claimMsg(final TxnContext txnContext, final long queueId, final long msgId) {
        return messageDao.claimMsg(txnContext, queueId, msgId);
    }

    public boolean unresolveMsg(final TxnContext txnContext, final long queueId, final long msgId) {
        return messageDao.unresolveMsg(txnContext, queueId, msgId);
    }

    public long unresolveAllClaimedMessages(final TxnContext txnContext, final long queueId) {
        return messageDao.unresolveAllClaimedMessages(txnContext, queueId);
    }

    public long unresolveAllClaimedMessages(final TxnContext txnContext) {
        return messageDao.unresolveAllClaimedMessages(txnContext);
    }

    public long releaseAllQueues(final TxnContext txnContext) {
        QClaimant claimant = txnContext.claimant();
        return txnContext.connection().update(QUEUE)
                .setNull(QUEUE.CLAIMANT)
                .where(QUEUE.CLAIMANT.eq(claimant.value()))
                .execute();
    }

    public long releaseQueue(final TxnContext txnContext, final long id) {
        QClaimant claimant = txnContext.claimant();
        return txnContext.connection().update(QUEUE)
                .setNull(QUEUE.CLAIMANT)
                .where(QUEUE.ID.eq(id).and(QUEUE.CLAIMANT.eq(claimant.value())))
                .execute();
    }

    public boolean releaseQueueIfEmpty(final TxnContext txnContext, final long id) {
        QClaimant claimant = txnContext.claimant();
        long affectedRows = txnContext.connection()
                .update(QUEUE)
                .setNull(QUEUE.CLAIMANT)
                .where(QUEUE.ID.eq(id).and(QUEUE.CLAIMANT.eq(claimant.value()).and(QUEUE.MESSAGE_COUNT.eq(0L))))
                .execute();
        return affectedRows == 1;
    }

    public long heartBeatQueue(final TxnContext txnContext, final long queueId) {
        long now = System.currentTimeMillis();
        long heartBeatThen = now - txnContext.claimantHeartBeatMillis();

        QClaimant claimant = txnContext.claimant();

        BooleanExpression claimantIsMeAndNotDead = QUEUE.CLAIMANT.eq(claimant.value()).and(QUEUE.CLAIMANT_TIME.loe(heartBeatThen));

        return txnContext.connection().update(QUEUE)
                .set(QUEUE.CLAIMANT_TIME, now)
                .where(QUEUE.ID.eq(queueId).and(claimantIsMeAndNotDead))
                .execute();
    }

    public void deleteMsg(final TxnContext txnContext, final long queueId, final long msgId) {
        messageDao.deleteMsg(txnContext, queueId, msgId);
    }

    private QueueDbTO toQueueDbTO(final Tuple tuple, long approxMsgCount) {
        Option<QTopic> topic = option(tuple.get(QUEUE.TOPIC)).map(s -> new QTopic(s));
        return new QueueDbTO(tuple.get(QUEUE.ID), tuple.get(QUEUE.NAME), tuple.get(QUEUE.PURPOSE), topic, tuple.get(QUEUE.CREATED_TIME), tuple.get(QUEUE.MODIFIED_TIME), approxMsgCount);
    }
}

# PSMQ - P*ss Simple Message Queuing #

###This repo has been forked and is now obsolete
For the cloud repo go here: https://stash.atlassian.com/projects/SDCLOUD/repos/psmq/browse
For the server repo go here: https://stash.atlassian.com/projects/SDSERVER/repos/psmq/browse


IT is a truth universally acknowledged, that a single application in possession of enough work must be in want of a message queue.

This is a simple queuing API that uses the backing database to queue messages.

The informing principle of this API is that things must be as p*ss simple as possible.

# Why PSMQ?

You may rightly ask why, given that there are many messaging systems out there, was PSMQ written?

The simple answer is around the ability to store messages in the database.  With Atlassian products moving to the cloud and
also enterprise clustering, the database is our best common area of communication for tenanted data.

When evaluating other Java based messaging systems, such as HornetQ, they all used specialised "journal files" on disk to get maximum throughput.  This, however,
is in direct opposition to our cloud and clustering architecture where local files are a no no.

While its eminently possible that the Cloud architecture can provide a common messaging service, this is not available in Server, no is
it inherently tenanted to customer data like the database is.  

Hence the need for a light weight, database backed messaging capability.

The final reason was around simplicity within product.  Atlassian products do many tasks internal to themselves that are naturally queueing
tasks.   Think outgoing email sending, incoming email processing, outgoing web hooks and so on.  These are all queueing in nature and PSMQ
can provide a simple, database backed, mechanism to write them in a generic and persistent manner.

# Using the API

PSMQ is based around the idea of a session.   `com.atlassian.psmq.api.QSession` is where most queuing operations are made.

You get a session from `com.atlassian.psmq.api.QSessionFactory` which is an OSGi exported component.  Use you favourite dependency injection
framework to get one.

        private QSession createSession()
        {
            QSessionDefinition sessionDefinition = QSessionDefinitionBuilder.newDefinition()
                    .withAutoCommitStrategy()
                    .build();
            return qSessionFactory.createSession(sessionDefinition);
        }

The above uses the most straight forward commit strategy of auto committing on every method.  Most of the time this is what you are going to
want to use.

From the session you can access `com.atlassian.psmq.api.queue.Queue`s and and the reading and writing interfaces such as `com.atlassian.psmq.api.message.QMessageConsumer`
and `com.atlassian.psmq.api.message.QMessageProducer`.

If the names sound familiar, then you might have encountered HornetQ before.  The naming and shape of the API is heavily inspired from it.

## Producing messages

Here is an example of creating a QMessageProducer so you can write some messages to a named queue.


        QSession qSession = createSession();

        QueueDefinition qDef = QueueDefinitionBuilder.newDefinition().withName("exampleQ").build();

        Queue exampleQ = qSession.queueOperations().accessQueue(qDef);

        QMessageProducer producer = qSession.createProducer(exampleQ);

        QMessage msg = buildTheMessage();

        producer.writeMessage(msg);


## Consuming messages

Conversely a QMessageConsumer works in a similar but opposite manner.  It reads messages from a queue


        QueueDefinition qDef = QueueDefinitionBuilder.newDefinition().withName("exampleQ").build();

        Queue exampleQ = qSession.queueOperations().accessQueue(qDef);

        QMessageConsumer consumer = qSession.createConsumer(exampleQ);

        Optional<QMessage> msg = consumer.claimAndResolve();

If there is a message in the queue at the time, then it will return it in the optional message.  If there is no
message then the optional message will be undefined.

You may ask why the consumer method is called `claimAndResolve()`?  That's because PSMQ uses a two stage 'claim' and then 'resolve'
reading strategy.  The above example does a claim and then resolve in one step.

The idea behind 'claim' and 'resolve' is to allow the reading of a message to be done in a controlled way.  Say for example you
are using a PSMQ message queue to send out web hooks messages.  Lets say you have a queue per web hook address.

If you use `claimAndResolve()` then you will want to be pretty sure that the web hook address you are contacting is up and ready.  If your
process fails during the time it takes to send the web hook message then the underlying event message will be lost.

However you can use explicit claiming to help solve this problem.  Step one is to claim a message of the queue and once its processed (or not)
you resolve it or unresolve it.


            QueueDefinition qDef = QueueDefinitionBuilder.newDefinition().withName("webhookQ").build();
            Queue webHookQ = qSession.queueOperations().accessQueue(qDef);
            QMessageConsumer consumer = qSession.createConsumer(webHookQ);

            Optional<QMessage> optionalMsg = consumer.claimMessage();
            if (optionalMsg.isPresent())
            {
                QMessage msg = optionalMsg.get();
                try
                {
                    sendWebHook(msg);
                    consumer.resolveClaimedMessage(msg);
                }
                catch (RuntimeException webHookProblems)
                {
                    consumer.unresolveClaimedMessage(msg);
                }
            }

When you `claim` a message, it is marked as read (so no one else can get it) but its resides in the queue still.

When you `resolve` the message, it is removed from the queue for good.  If you `unresolve` the message then it is made
available to the queue again for reading by yourself or any other `QMessageConsumer` who might be accessing that queue.

The retry strategy code is up to you.  PSMQ gives you a basic two step mechanism to allow you to do something clever.

You might want to try a retry library like [https://github.com/rholder/guava-retrying](https://github.com/rholder/guava-retrying) and back
it with an ExecutorService say.

## Exclusive read cycle

By means of the `releaseQueueIfEmpty()` method, PSMQ can be used to implement an exclusive read cycle where a single 
reader can keep exclusive access to the queue until all messages are exhausted. The queue size check and the release 
operations are executed atomically, thus guaranteeing that the current claimant relinquishes its exclusive access to
the queue only if there are not messages left to process.

The exclusive access semantics can be used when multiple claimants might be trying to access the same queue 
concurrently, and it is crucial that the claimant gaining access to the queue only releases it when no messages are 
left in it.


            QueueDefinition qDef = QueueDefinitionBuilder.newDefinition().withName("webhookQ").build();
            Optional<Queue> queueOpt = qSession.queueOperations().exclusiveAccessQueue(qDef);
            if (!queueOpt.isPresent) {
                // Could not get exclusive access to the queue, another claimant is holding it.
                // Do nothing, or handle it appropriately based on your business logic
                return;
            }
            
            QMessageConsumer consumer = qSession.createConsumer(queue);
            
            do {
                consumer.claimAndResolve().ifPresent(msg -> {
                        // Process message
                });
            } while (!qSession.queueOperations().releaseQueueIfEmpty(queue));


## Claimants and recovering from claim failures

The claim pattern allows you to recover if the power goes out on your process.  You can unresolve any messages that have been claimed by yourself
but haven't be resolved as processed.  The trick is knowing which messages are yours.

You do this by registering as a "claimant" when you start your `QSession`.

        QClaimant claimant = QClaimant.claimant("myNameThatMeansSomethingToMe");
        QSessionDefinition sessionDefinition = QSessionDefinitionBuilder.newDefinition().withAutoCommitStrategy().withClaimant(claimant).build();
        QSession qSession = qSessionFactory.createSession(sessionDefinition);

All messages claimed after this will be claimed in this name.  Now lets assume that the power is cut during your processing run.

What you typically do when restarting your processing code is

* Connect as a well known claimant
* Unresolve any previous claimed messages associated with that name
* Then start processing as normal

You will then get any read messages that were in the queue marked as claimed but subsequently failed to be processed via this pattern.

        qSession.queueOperations().unresolveAllClaimedMessages();

You can do this on a specific queue as well if you like.

        Queue someQ = qSession.queueOperations().accessQueue(QueueDefinitionBuilder.newDefinition().withName("someQ").build());
        qSession.queueOperations().unresolveAllClaimedMessages(someQ);

The other function that claimants give you is exclusive access to queues.  Say you have a 3 node cluster and you only want one node to
process the outbound email queue.  What you do is have have each node claim the queue using the same 'claimant' name.

The first to access the queue (and have it go from having no claimant to some claimant) will gain access.  The others will miss out.

        QClaimant nodeClaimant = QClaimant.claimant("outbound_email_processor");
        QSession qMailSession = qSessionFactory.createSession(QSessionDefinitionBuilder.newDefinition()
                .withClaimant(claimant).build());
        Optional<Queue> outboundQ = qMailSession.queueOperations().exclusiveAccessQueue(QueueDefinitionBuilder.newDefinition().withName("outboundQ").build());
        if (outboundQ.isPresent()) {
            // right we go exclusive access - lets process things
            processOutboundEmails(qMailSession,outboundQ.get());
        }

Since you have exclusive access to the queue, and therefore its messages, you can unresolve all the messages that failed to be processed.  Note you
must setup you code to use the same "claimant" name for this pattern to work.


## Publishing messages via Topics

A queue can be accessed and associated with a `com.atlassian.psmq.api.queue.QTopic`.  A topic is just a named conversation if you will.

If you open a `QMessageProducer` that is associated with a topic then any other defined queue that is also associated with this
topic will have messages placed into it.  That way you can have multiple `QMessageConsumers` reading from different queues
that all get the same message on the same topic, all from a single message put.


        QTopic trainTopic = QTopic.newTopic("trainSpotting");

        QueueDefinition qDef1 = QueueDefinitionBuilder.newDefinition()
                .withName("chooChooQueue")
                .withTopic(trainTopic)
                .build();
        Queue chooChooQueue = qSession.queueOperations().accessQueue(qDef1);
        QMessageConsumer chooChooConsumer = qSession.createConsumer(chooChooQueue);

        QueueDefinition qDef2 = QueueDefinitionBuilder.newDefinition()
                .withName("tootTootQueue")
                .withTopic(trainTopic)
                .build();
        Queue tootTootQueue = qSession.queueOperations().accessQueue(qDef2);
        QMessageConsumer tootTootConsumer = qSession.createConsumer(chooChooQueue);


        QMessageProducer producer = qSession.createProducer(trainTopic);
        producer.writeMessage(buildMsg("Thomas The Tank Engine"));


In the example above the `chooChooConsumer` and `tootTootConsumer` will both read the same message.

## QMessage Structure

A `com.atlassian.psmq.api.message.QMessage` has attributes and properties associated with it as well as the message data.

The key attributes are

* Content Type

This is up to you as the user of the API to interpret.  Its a hint to you on how to parse the message.

* Content Version

This is a very simple mechanism to allow you to version messages.  You MUST assume that multiple versions
of your messages may be in the same queue if you are a cloud native application, following fast-five principles.

* Priority

Messages with the highest priority are read first.  Then by age.

* Expiry Date (optional)

A message that has expired will not be read from the queue.  You don't have to use expiry but you might want to consider it pretty strongly
as messages that live for ever in a queue are not or much use.

* Message Id (optionally of your choosing)

You don't have to use a message id.  There are some use cases where it is useful to read by specific message id but in general
you should avoid it if you dont need it.

A message can also have a set of properties associated with it.  This is an unbounded set of named strings, longs, dates or booleans and
it is collective represented by `com.atlassian.psmq.api.property.QPropertySet`.  As the user of the API, it is up to you to decide
what these named properties represent.

The following is an example of building a QMessage with the lot!

        QMessageBuilder builder = QMessageBuilder.newMsg();
        //
        // there are some pre-build constants that you can use
        // but how you interpret then is up to you
        builder.withContentType(QContentType.JSON);
        builder.withContentVersion(QContentVersion.V0);
        //
        // in 5 days this message will expire and will no longer
        // be read from the queue.  It disappears into the aether.
        builder.withExpiryFromNow(TimeUnit.DAYS.toMillis(5));

        // it does not have to be unique - that's up to you
        // you also don't have to use it.  One will be generated
        // if you don't specify one
        builder.withId(QMessageId.id("someIdOfYourChoosing"));

        //
        // Properties have names and types and can be strings, longs
        // dates or booleans
        builder.withProperty("firstNamedProperty", "stringValue")
                .withProperty("secondNamedProperty", 666)
                .withProperty("thirdNamedProperty", new Date())
                .withProperty("fourthNamedProperty", true);

        //
        // You can use QBuffer to append any sort of data into the message.  Go wild!
        builder.newBuffer()
                .append("Some")
                .append("message")
                .append("data")
                .append("including some specific types such as")
                .append(5f).append(5).append(5L)
                .append(true)
                .append(" and so on.  Read the JavaDoc for more examples!")
                .buildBuffer();

        QMessage msgWithTheLot = builder.build();


### How do I get set up? ###

* This is atlas-debug based.  

Run it up in your favourite Atlassian host application

* How to run tests

    To run with Postgres (Needed for Jira Cloud): 
    
        mvn clean install
        cd tests/tpm/dbconfig
        mvn verify -PPostgres
        cd ../jirasetup/
        mvn verify -PJirasetup
        cd ../../integration-tests
        mvn verify -Pintegration-tests,ci-realdb

    To run with the default database (if using server, don't forget the -Pserver arg):
    
        cd  psmq/tests/integration-tests
        mvn clean install -Pintegration-tests


### Contribution guidelines ###

* Writing tests
    yes
* Code review
    yes
* Other guidelines
    Remember the informing principles.  If its not P*ss Simple, its not PSMQ.

### Who do I talk to? ###

* Brad Baker is the spiritual owner of this code (for now)


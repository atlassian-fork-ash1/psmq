package com.atlassian.psmq.internal.rest.implementation;

import com.atlassian.psmq.api.queue.Queue;
import com.atlassian.psmq.internal.util.rest.JsonOut;

import java.io.IOException;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import static java.lang.String.valueOf;

/**
 * Writes REST links into a response
 */
public class LinksWriter
{
    private final UriInfo uriInfo;
    private final JsonOut jsonOut;

    public LinksWriter(final UriInfo uriInfo, JsonOut jsonOut)
    {
        this.uriInfo = uriInfo;
        this.jsonOut = jsonOut;
    }

    class BasicLink
    {

        UriBuilder uri()
        {
            return uriInfo.getBaseUriBuilder();
        }

        UriBuilder selfUri()
        {
            return uriInfo.getAbsolutePathBuilder();
        }

    }

    @SuppressWarnings ("UnusedDeclaration")
    class ApiLinks extends BasicLink
    {
        private final String self;
        private final String api;
        private final String queues;
        private final String messages;
        private final String put;
        private final String get;

        public ApiLinks()
        {
            self = selfUri().build().toASCIIString();
            api = uri().path("api").build().toASCIIString();
            queues = uri().path("queues").build().toASCIIString();
            messages = queues + "/{queueId}/messages";
            put = queues + "/{queueId}/messages/put";
            get = queues + "/{queueId}/messages/get";
        }
    }

    @SuppressWarnings ("UnusedDeclaration")
    class QueueLinks extends BasicLink
    {
        private final String self;
        private final String api;
        private final String queues;
        private final String messages;
        private final String put;

        private final String get;

        public QueueLinks(Queue queue)
        {
            String qId = valueOf(queue.id());

            api = uri().path("api").build().toASCIIString();
            queues = uri().path("queues").build().toASCIIString();

            self = uri().path("queues").path(qId).build().toASCIIString();
            messages = self + "/messages";
            get = self + "/messages/get";
            put = self + "/messages/put";
        }
    }


    public void buildApiLinks()
    {
        rte(() -> {
            jsonOut.name("links");
            jsonOut.toJson(new ApiLinks());
        });
    }

    public void buildQueueLinks(Queue queue)
    {
        rte(() -> {
            jsonOut.toJson("links", new QueueLinks(queue));
        });
    }

    private static interface IORunnable
    {
        void run() throws IOException;
    }

    private void rte(IORunnable runnable)
    {
        try
        {
            runnable.run();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

    }
}

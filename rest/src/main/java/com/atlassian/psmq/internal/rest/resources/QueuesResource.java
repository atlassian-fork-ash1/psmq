package com.atlassian.psmq.internal.rest.resources;


import com.atlassian.fugue.Option;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.psmq.internal.rest.implementation.QueuesRestController;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import static com.atlassian.fugue.Option.option;
import static com.atlassian.psmq.internal.util.rest.RestKit.parseLong;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

/**
 */
@Produces (MediaType.APPLICATION_JSON)
@Path ("queues")
@AnonymousAllowed  // TODO baggins - for now to make testing easier
public class QueuesResource
{
    private final QueuesRestController queuesRestController;

    public QueuesResource(final QueuesRestController queuesRestController)
    {
        this.queuesRestController = queuesRestController;
    }

    @GET
    public Response getQueues(@Context UriInfo uriInfo)
    {
        return queuesRestController.getAllQueues(uriInfo);
    }

    @POST
    public Response createQueue(@FormParam ("name") String name, @FormParam ("purpose") String purpose, @Context UriInfo uriInfo)
    {
        return queuesRestController.createQueue(uriInfo, name, purpose);
    }

    @Path ("{queueIdOrKey}")
    @GET
    public Response getSpecificQueue(@PathParam ("queueIdOrKey") String queueIdOrKey, @Context UriInfo uriInfo)
    {
        if (option(queueIdOrKey).isDefined())
        {
            Option<Long> queueId = parseLong(queueIdOrKey);
            if (queueId.isDefined())
            {
                return queuesRestController.getQueueById(uriInfo, queueId.get());
            }
            else
            {
                return queuesRestController.getQueueByName(uriInfo, queueIdOrKey);
            }
        }
        return Response.status(NOT_FOUND).build();
    }

    @Path ("{queueId}/messages")
    @GET
    public Response browseMessages(@PathParam ("queueId") String queueIdOrKey, @Context UriInfo uriInfo, @Context HttpHeaders httpHeaders)
    {
        Option<Long> queueId = parseLong(queueIdOrKey);
        if (queueId.isDefined())
        {
            return queuesRestController.browseQueueMessagesById(uriInfo, httpHeaders, queueId.get());
        }
        return Response.status(NOT_FOUND).build();
    }

    @POST
    @Path ("{queueId}/messages/put")
    @Consumes(MediaType.TEXT_PLAIN)
    public Response putPlainMessage(@PathParam ("queueId") String queueIdOrKey, String msg, @Context UriInfo uriInfo) {
        Option<Long> queueId = parseLong(queueIdOrKey);
        if (queueId.isDefined())
        {
            return queuesRestController.putSimpleMessage(uriInfo, queueId.get(), msg);
        }
        return Response.status(NOT_FOUND).build();
    }

    @POST
    @Path ("{queueId}/messages/put")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putComplexMessage(@PathParam ("queueId") String queueIdOrKey, String jsonInput, @Context UriInfo uriInfo)
    {
        Option<Long> queueId = parseLong(queueIdOrKey);
        if (queueId.isDefined())
        {
            return queuesRestController.putComplexMessage(uriInfo, queueId.get(), jsonInput);
        }
        return Response.status(NOT_FOUND).build();
    }

    @POST
    @Path ("{queueId}/messages/get")
    public Response getMessage(@PathParam ("queueId") String queueIdOrKey, @Context UriInfo uriInfo)
    {
        Option<Long> queueId = parseLong(queueIdOrKey);
        if (queueId.isDefined())
        {
            return queuesRestController.getNextMessage(uriInfo, queueId.get());
        }
        return Response.status(NOT_FOUND).build();
    }

}

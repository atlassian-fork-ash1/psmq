package com.atlassian.psmq.internal.rest.resources;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.psmq.internal.rest.implementation.QueuesRestController;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 */
@Path("/api")
@Produces (MediaType.APPLICATION_JSON)
@AnonymousAllowed  // TODO baggins - for now to make testing easier
public class ApiResource
{
    private final QueuesRestController queuesRestController;

    public ApiResource(final QueuesRestController queuesRestController)
    {
        this.queuesRestController = queuesRestController;
    }

    @GET
    public Response api(@Context UriInfo uriInfo)
    {
        return queuesRestController.api(uriInfo);
    }
}
